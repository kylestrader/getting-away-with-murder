﻿using UnityEngine;
using System.Collections;

public class PlayerObjectInteract : MonoBehaviour {

	// this script pushes all rigidbodies that the character touches
	float pushPower = 2.0f;
	public void OnControllerColliderHit (ControllerColliderHit hit) {
		Rigidbody body = hit.collider.attachedRigidbody;

		// no rigidbody
        if (body == null || System.Object.ReferenceEquals(body.gameObject.GetComponent<FurnitureScript>(), null))
			return;

        if (body.gameObject.GetComponent<FurnitureScript>().isHeld || body.gameObject.GetComponent<FurnitureScript>().mGravityDelayed)
            return;
			
		// We dont want to push objects below us
		if (hit.moveDirection.y < -0.3) 
			return;
		
		// Calculate push direction from move direction, 
		// we only push objects to the sides never up and down
        body.gameObject.GetComponent<FurnitureScript>().pickThisUp();
		Vector3 pushDir = new Vector3 (hit.moveDirection.x, 0, hit.moveDirection.z);

		// If you know how fast your character is trying to move,
		// then you can also multiply the push velocity by that.
		
		// Apply the push
		body.velocity = pushDir * pushPower;
	}
}
