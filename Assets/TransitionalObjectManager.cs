﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TransitionalObjectManager : MonoBehaviour {

    public List<string> TransitionalObjects;

    public void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void ProtectObjectsFromLoad()
    {
        List<GameObject> objs = CollectItems();

        foreach (GameObject g in objs)
        {
            DontDestroyOnLoad(g);
        }
    }

    public void DestroyObjectsForLoad()
    {
        List<GameObject> objs = CollectItems();

        foreach (GameObject g in objs)
        {
            Destroy(g);
        }
    }

    private List<GameObject> CollectItems()
    {
        if (TransitionalObjects.Count > 0)
        {
            List<GameObject> objs = new List<GameObject>();
            foreach (string s in TransitionalObjects)
            {
                objs.Add(GameObject.Find(s));
            }

            return objs;
        }

        return null;
    }
}
