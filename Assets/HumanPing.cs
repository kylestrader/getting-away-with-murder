﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HumanPing : MonoBehaviour {

    public GameObject Game_Env_Mgr;

    public enum WayPointType
    {
        WP_ARROW = 0,
        WP_BODY = 1,
        WP_LOG = 2,
        WP_FIRE_PLACE = 3,
        WP_MATCHES = 4,
        WP_GAS = 5
    }

    public class m_WayPointMeta
    {
        public m_WayPointMeta(WayPointType type, Transform loc, float lifetime, float curlife)
        {
            wType = type;
            wLoc = loc;
            wLifeTime = lifetime;
            wCurLife = 0f;
            wTransparency = 1f; //0 - 1
        }

        public void setCurLife(float curlife)
        {
            wCurLife = curlife;
            wTransparency = 1f - (wCurLife / wLifeTime);
        }

        public WayPointType wType;
        public Transform wLoc;
        public float wLifeTime;
        public float wCurLife;
        public float wTransparency;
    }

    public List<Sprite> mWayPoints;
    private int mWP_ID = 0;

    //ID represents the waypoint life time and vector
    private Dictionary<int, m_WayPointMeta> mWayPointDictionary;

    public float mGenericWayPointLifeTime = 4f;
    public float mGenericFadeTime = 2f;

    public GameObject mUI;

    public Camera mCamera;

	// Use this for initialization
	void Start () {
        Game_Env_Mgr = GameObject.Find("_GAME_ENV_MANAGER");
        mWayPointDictionary = new Dictionary<int, m_WayPointMeta>();
        if (System.Object.ReferenceEquals(Game_Env_Mgr, null))
            Debug.LogError("Game environment manager is unknown!");
	}

    public void Update()
    {
        if (mWayPointDictionary != null && mWayPointDictionary.Count > 0)
        {
            List<int> indices = new List<int>();
            foreach (KeyValuePair<int, m_WayPointMeta> entry in mWayPointDictionary)
            {
                if (entry.Value.wLoc == null)
                {
                    indices.Add(entry.Key);
                }

                else if (entry.Value.wCurLife >= entry.Value.wLifeTime)
                {
                    indices.Add(entry.Key);
                }

                else
                {
                    m_WayPointMeta w = entry.Value;
                    w.setCurLife(entry.Value.wCurLife + Time.deltaTime);
                }

            }

            foreach (int i in indices)
            {
                mWayPointDictionary.Remove(i);
            }
        }
    }

    void OnGUI()
    {
        if (mWayPointDictionary != null && mWayPointDictionary.Count > 0)
        {
            foreach (KeyValuePair<int, m_WayPointMeta> entry in mWayPointDictionary)
            {
                if (entry.Value.wLoc != null)
                {
                    Sprite draw = FindImageByName("Arrow");
                    //this is expecting the transform of the outline object in the pinged object
                    Transform t = entry.Value.wLoc;
                    Color newColor = t.gameObject.GetComponent<MeshRenderer>().material.GetColor("_Color");
                    t.gameObject.GetComponent<MeshRenderer>().material.SetColor("_Color", new Color(newColor.r, newColor.g, newColor.b, entry.Value.wTransparency));
                    Vector3 screenPos = Camera.main.WorldToScreenPoint(mWayPointDictionary[entry.Key].wLoc.position + (Vector3.up * 3));
                    if (screenPos.z < 0)
                    {
                        screenPos *= -1;
                    }
                    Vector2 screenTruncatedPos = new Vector2(screenPos.x, screenPos.y);
                    Rect screenRect = new Rect(screenTruncatedPos.x - (draw.rect.width / 2), Screen.height - (screenTruncatedPos.y + (draw.rect.height / 2)), draw.rect.width, draw.rect.height);
                    Vector3 toObj = (t.position - transform.position).normalized;
                    float angle = Vector3.Angle(mCamera.transform.forward.normalized, toObj.normalized);
                    if (angle < (mCamera.fieldOfView / 1.5f))
                    {
                        GUI.color = new Color(1.0f, 1.0f, 1.0f, entry.Value.wTransparency);
                        GUI.DrawTexture(screenRect, draw.texture, ScaleMode.ScaleToFit, true);
                        GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                    }
                }
            }
        }
    }

    public void SendPing(int ID, WayPointType type)
    {
        GetComponent<PhotonView>().RPC("NetworkReceivePing", PhotonTargets.AllViaServer, ID, (int)type, mGenericWayPointLifeTime);

		switch(type)
		{
			case WayPointType.WP_LOG:
			{
				GetComponent<PhotonView>().RPC ("networkedPlaySound", PhotonTargets.AllBufferedViaServer, "wood");
				break;
			}
			case WayPointType.WP_MATCHES:
			{
				GetComponent<PhotonView>().RPC ("networkedPlaySound", PhotonTargets.AllBufferedViaServer, "matches");
				break;
			}
			case WayPointType.WP_ARROW:
			{
				GetComponent<PhotonView>().RPC ("networkedPlaySound", PhotonTargets.AllBufferedViaServer, "idle");
				break;
			}
			case WayPointType.WP_GAS:
			{
				GetComponent<PhotonView>().RPC ("networkedPlaySound", PhotonTargets.AllBufferedViaServer, "gas");
				break;
			}
			case WayPointType.WP_BODY:
			{
				GetComponent<PhotonView>().RPC ("networkedPlaySound", PhotonTargets.AllBufferedViaServer, "body");
				break;
			}
		}

    }

    [RPC]
    public void NetworkReceivePing(int ID, int type, float lifeTime)
    {
        Transform newTrans;
        switch((WayPointType)type)
        {
            case WayPointType.WP_ARROW:
                newTrans = Game_Env_Mgr.GetComponent<GameEnvironment>().getHumanWithID(ID).transform.Find("Outline");
                break;
            case WayPointType.WP_BODY:
                newTrans = Game_Env_Mgr.GetComponent<GameEnvironment>().getBodyWithID(ID).transform.Find("Outline");
                break;
            case WayPointType.WP_FIRE_PLACE:
                newTrans = Game_Env_Mgr.GetComponent<GameEnvironment>().getFirePlaceWithID(ID).transform.Find("Outline");
                break;
            case WayPointType.WP_GAS:
                newTrans = Game_Env_Mgr.GetComponent<GameEnvironment>().getGasolineWithID(ID).transform.Find("Outline");
                break;
            case WayPointType.WP_MATCHES:
                newTrans = Game_Env_Mgr.GetComponent<GameEnvironment>().getMatchesWithID(ID).transform.Find("Outline");
                break;
            case WayPointType.WP_LOG:
                newTrans = Game_Env_Mgr.GetComponent<GameEnvironment>().getFireWoodWithID(ID).transform.Find("Outline");
                break;
            default:
                Debug.LogError("Type could not be determined!");
                return;
        }

        List<int> tmp = new List<int>();
        foreach (KeyValuePair<int, m_WayPointMeta> entry in mWayPointDictionary)
        {
            if (entry.Value.wLoc == newTrans)
                tmp.Add(entry.Key);
        }

        foreach (int i in tmp)
        {
            mWayPointDictionary[i].wCurLife = mWayPointDictionary[i].wLifeTime;
        }

        m_WayPointMeta meta = new m_WayPointMeta((WayPointType)type, newTrans, lifeTime, 0f);
        mWayPointDictionary.Add(mWP_ID, meta);
        mWP_ID++;
    }

    public Sprite FindImageByName(string name)
    {
        foreach (Sprite img in mWayPoints)
        {
            if (img.name == name)
                return img;
        }

        return null;
    }

    public Sprite FindImageByType(WayPointType type)
    {
        switch(type)
        {
            case WayPointType.WP_ARROW:
                return FindImageByName("Arrow");
            case WayPointType.WP_BODY:
                return FindImageByName("body");
            case WayPointType.WP_FIRE_PLACE:
                return FindImageByName("fireplace");
            case WayPointType.WP_GAS:
                return FindImageByName("gas");
            case WayPointType.WP_MATCHES:
                return FindImageByName("matches");
            case WayPointType.WP_LOG:
                return FindImageByName("log");
        }
        return null;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}
