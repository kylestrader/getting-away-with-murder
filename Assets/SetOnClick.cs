﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetOnClick : MonoBehaviour {

	public bool isGhost;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Button>().onClick.AddListener(() => GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<PlayerCreate>().SpawnMyPlayer(isGhost));
	}
}
