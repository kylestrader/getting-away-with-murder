﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {

    GameObject transObjMgr;
	public RoomInfo[] rooms;

    public void Awake()
    {
        Application.targetFrameRate = 60;
    }
	// Use this for initialization
	void Start (){
        transObjMgr = GameObject.Find("_TRANSITIONAL_OBJECTS_MANAGER");
		Connect ();
	}

	void Connect(){
		Debug.Log ("Attempting network connect");
		PhotonNetwork.ConnectUsingSettings ("test");
	}

	void OnGUI (){
		GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString() );
	}

	void OnJoinedLobby(){
		PopulateRoomList ();
	}

	void PopulateRoomList()
	{
		/*rooms = PhotonNetwork.GetRoomList ();
		Debug.Log ("Generated room list. " + rooms.Length + " rooms online.");
		GameObject.Find ("_MENU_SYSTEM/MainMenu").GetComponent<MenuActionHandler> ().rooms = rooms;*/
	}

	void OnPhotonRandomJoinFailed() {
		PhotonNetwork.CreateRoom (null);
	}

	void OnJoinedRoom() {
        GameObject.Find("_GAME_ENV_MANAGER").GetComponent<GameEnvironment>().Instantiate();
		GameObject.Find ("MainMenu").SetActive (false);
		GameObject.Find ("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("PlayerSelect").SetActive (true);
        //if (PhotonNetwork.isMasterClient) GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().PickFireplaceSpawn();
	}

	public void Disconnect()
	{
        PhotonNetwork.Disconnect();
        transObjMgr.GetComponent<TransitionalObjectManager>().DestroyObjectsForLoad();
        Destroy(transObjMgr);
        transObjMgr.GetComponent<LevelLoader>().LoadLevel("MainMenu", false);
	}

	public void JoinRandomRoom()
	{
        if (PhotonNetwork.JoinRandomRoom())
        {
            transObjMgr.GetComponent<TransitionalObjectManager>().ProtectObjectsFromLoad();
            transObjMgr.GetComponent<LevelLoader>().LoadLevel("TestScene", true);
			//Application.LoadLevel("TestScene");
        }
	}

	public void JoinRoom(string name)
	{
		bool nameExists = false;
		RoomInfo[] rooms = PhotonNetwork.GetRoomList ();
		foreach(RoomInfo r in rooms)
		{
			string roomName = r.name;
			if (roomName == name)
			{
				nameExists = true;
			}
		}

        if (nameExists)
        {
            if (PhotonNetwork.JoinRoom(name))
            {
                transObjMgr.GetComponent<TransitionalObjectManager>().ProtectObjectsFromLoad();
                transObjMgr.GetComponent<LevelLoader>().LoadLevel("TestScene", true);
				//Application.LoadLevel("TestScene");
            }
            else
                Debug.Log("Could not join network room");
        }
        else JoinRandomRoom();
	}

	public void CreateRoom(string name, bool visibility, bool isOpen, bool isPasswordProtected, string password, int maxPlayers)
	{

		PhotonNetwork.CreateRoom (name, visibility, isOpen, maxPlayers, null, null);
        Debug.Log("Created room with name " + name);
		transObjMgr.GetComponent<TransitionalObjectManager>().ProtectObjectsFromLoad();
        transObjMgr.GetComponent<LevelLoader>().LoadLevel("TestScene", true);
		//Application.LoadLevel("TestScene");
	}
}
