﻿using UnityEngine;
using System.Collections;

public class HolyWaterPickup : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void AddVial(GameObject player)
    {
        player.GetComponent<HumanControls>().numVials++;
    }

    bool CheckVialsFull(GameObject player)
    {
        if (player.GetComponent<HumanControls>().numVials < player.GetComponent<HumanControls>().maxNumVials)
            return false;
        else return true;
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Human")
        {
            if (!CheckVialsFull(other.gameObject))
            {
                AddVial(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
