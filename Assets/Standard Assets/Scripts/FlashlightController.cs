﻿using UnityEngine;
using System.Collections;

public class FlashlightController : MonoBehaviour {

	public Light flashlightObj;
	public float maxFlashlightAngle = 120f;
	public int numFramesToMax = 7;

	// Use this for initialization
	void Start () {
		flashlightObj.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (flashlightObj.enabled && flashlightObj.spotAngle < maxFlashlightAngle) {
			float angle = flashlightObj.spotAngle;
			angle += (maxFlashlightAngle / (float)numFramesToMax);
			flashlightObj.spotAngle = angle;
		}
	}
}
