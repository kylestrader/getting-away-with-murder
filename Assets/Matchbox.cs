﻿using UnityEngine;
using System.Collections;

public class Matchbox : Item {
    private bool pickedUp;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
    void Update()
    {
        if (GetComponent<PhotonView>().isMine && pickedUp)
        {
            GameObject.Find("_GAME_ENV_MANAGER").GetComponent<PhotonView>().RPC("removeMatches", PhotonTargets.AllBufferedViaServer, GetComponent<GameId>().ID);
            PhotonNetwork.Destroy(gameObject);
        }
		
	}
	
	public override void PickUp(HumanControls player)
	{
		if (!player.inventory.hasMatches)
        {
            pickedUp = true;
            if (!GetComponent<PhotonView>().isMine)
                gameObject.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
			player.inventory.hasMatches = true;
			player.PlayObjectiveSound();
		}
	}
}
