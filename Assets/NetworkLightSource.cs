﻿using UnityEngine;
using System.Collections;

public class NetworkLightSource : MonoBehaviour {

	public bool canTurnOn;

	public float maxCooldown = 3.0f;
	public float cooldownTimer;
	public bool cooldownActive = false;
	//public float 

	// Use this for initialization
	void Start () {

		canTurnOn = true;
	
	}
	
	// Update is called once per frame
	void Update () {

		TrackCooldown ();
	
	}

	void TrackCooldown()
	{
		if (cooldownActive)
		{
			if (cooldownTimer > 0) cooldownTimer -= Time.deltaTime;
			else if (cooldownTimer <= 0) EnableLight ();
		}
	}

	public void DisableLight()
	{
		GetComponent<PhotonView>().RPC("networkDisableLightSource", PhotonTargets.AllBufferedViaServer);
		canTurnOn = false;

		cooldownTimer = maxCooldown;
		cooldownActive = true;
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
	}

	public void EnableLight()
	{
		GetComponent<PhotonView>().RPC("networkEnableLightSource", PhotonTargets.AllBufferedViaServer);
		canTurnOn = true;
		cooldownActive = false;
	}

	[RPC]
	public void networkDisableLightSource()
	{
        GetComponent<Light>().enabled = false; 
        
        foreach (Transform child in transform)
        {
            child.GetComponent<ParticleSystem>().Stop();
        }
		canTurnOn = false;
	}

	[RPC]
	public void networkEnableLightSource()
	{
		GetComponent<Light> ().enabled = true;
        
        foreach (Transform child in transform)
        {
            child.GetComponent<ParticleSystem>().Play();
        }
		canTurnOn = true;
	}
}
