﻿using UnityEngine;
using System.Collections;

public class Tinder : Item {
    private bool pickedUp;
	// Use this for initialization
	void Start () {
        pickedUp = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<PhotonView>().isMine && pickedUp)
        {
            GameObject.Find("_GAME_ENV_MANAGER").GetComponent<PhotonView>().RPC("removeGasoline", PhotonTargets.AllBufferedViaServer, GetComponent<GameId>().ID);
            PhotonNetwork.Destroy(gameObject);
        }
	}
	
	public override void PickUp(HumanControls player)
	{
		if (!player.inventory.hasTinder)
        {
            pickedUp = true;
            if (!GetComponent<PhotonView>().isMine)
                gameObject.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
			player.inventory.hasTinder = true;
            player.PlayObjectiveSound();
		}
	}
}
