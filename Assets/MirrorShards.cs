﻿using UnityEngine;
using System.Collections;

public class MirrorShards : MonoBehaviour {

	public bool exists;

	// Use this for initialization
	void Start () {
		exists = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (exists)
		{
			GetComponent<MeshRenderer>().enabled = true;
		}
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Human")
		{
			other.gameObject.GetComponent<HumanControls>().isSlowed = true;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Human")
		{
			other.gameObject.GetComponent<HumanControls>().isSlowed = false;
		}
	}

}
