﻿using UnityEngine;
using System.Collections;

public class DisconnectMe : MonoBehaviour {

    public void Disconnect()
    {
        GameObject personalData = GameObject.Find("_PERSONAL_GAME_DATA");
        personalData.GetComponent<MyData>().HandleDisconnect();
    }
}
