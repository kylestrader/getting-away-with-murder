﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelLoader : MonoBehaviour {

    public Texture2D emptyProgressBar; // Set this in inspector.
    public Texture2D fullProgressBar; // Set this in inspector.
    public Texture2D loadScreen;

    private AsyncOperation async = null; // When assigned, load is in progress.

    bool shouldDrawLoadBar = false;

    public void LoadLevel(string SceneName, bool drawLoadBar)
    {
        Application.LoadLevel(SceneName);
        //StartCoroutine(LoadALevel(SceneName));
        shouldDrawLoadBar = drawLoadBar;
    }

    private IEnumerator LoadALevel(string levelName)
    {
        async = Application.LoadLevelAsync(levelName);
        yield return async;
    }

    void OnGUI()
    {
        if (async != null && shouldDrawLoadBar)
        {
            Debug.Log(async.progress);
            GUI.DrawTexture(new Rect(0, 0, loadScreen.width, loadScreen.height), loadScreen);
            GUI.DrawTexture(new Rect(0, 0, emptyProgressBar.width, emptyProgressBar.height), emptyProgressBar);
            GUI.DrawTexture(new Rect(0, 0, emptyProgressBar.width * async.progress, emptyProgressBar.height), fullProgressBar);

            if (async.isDone)
            {
                async = null;
            }
        }
    }

}
