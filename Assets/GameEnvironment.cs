﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameEnvironment : Photon.MonoBehaviour {

    public GameObject Body_ENV;
    public List<GameObject> FireWood_ENV;
    public List<GameObject> FirePlaces_ENV;
    public List<GameObject> Matches_ENV;
    public List<GameObject> Gasoline_ENV;

    private List<GameObject> woodSpawns;
    private List<GameObject> gasSpawns;
    private List<GameObject> matchSpawns;

    public Dictionary<int, GameObject> Humans;
    public Dictionary<int, GameObject> Ghosts;
    public Dictionary<int, GameObject> Bodies;
    public Dictionary<int, GameObject> FireWood;
    public Dictionary<int, GameObject> FirePlaces;
    public Dictionary<int, GameObject> Matches;
    public Dictionary<int, GameObject> Gasoline;

    private int _HumanIDCount;
    private int _GhostIDCount;
    private int _BodyIDCount;
    private int _FireWoodIDCount;
    private int _FirePlaceIDCount;
    private int _MatchesIDCount;
    private int _GasolineIDCount;

    #region Utils
    public void Instantiate()
    {
        if (System.Object.ReferenceEquals(null, Body_ENV))
            Debug.Log("Body_ENV is not referenced in Game Environment Manager!");

        if (System.Object.ReferenceEquals(null, FireWood_ENV) || FireWood_ENV.Count <= 0)
            Debug.Log("FireWood_ENV is not referenced in Game Environment Manager!");

        if (System.Object.ReferenceEquals(null, FireWood_ENV) || FirePlaces_ENV.Count <= 0)
            Debug.Log("FireWood_ENV is not referenced in Game Environment Manager!");

        if (System.Object.ReferenceEquals(null, Matches_ENV) || Matches_ENV.Count <= 0)
            Debug.Log("Matches_ENV is not referenced in Game Environment Manager!");

        if (System.Object.ReferenceEquals(null, Gasoline_ENV) || Gasoline_ENV.Count <= 0)
            Debug.Log("Gasoline_ENV is not referenced in Game Environment Manager!");

        _HumanIDCount = 0;
        _GhostIDCount = 0;
        _BodyIDCount = 0;
        _FireWoodIDCount = 0;
        _FirePlaceIDCount = 0;
        _MatchesIDCount = 0;
        _GasolineIDCount = 0;

        Humans = new Dictionary<int, GameObject>();
        Ghosts = new Dictionary<int, GameObject>();
        Bodies = new Dictionary<int, GameObject>();
        FireWood = new Dictionary<int, GameObject>();
        FirePlaces = new Dictionary<int, GameObject>();
        Matches = new Dictionary<int, GameObject>();
        Gasoline = new Dictionary<int, GameObject>();

        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("IS MASTER CLIENT");
            woodSpawns = new List<GameObject>(GameObject.FindGameObjectsWithTag("WoodSpawn"));
            gasSpawns = new List<GameObject>(GameObject.FindGameObjectsWithTag("GasSpawn"));
            matchSpawns = new List<GameObject>(GameObject.FindGameObjectsWithTag("MatchSpawn"));

            //Spawn Body
            GameObject body = Body_ENV;
            body.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
            spawnBody(body);
            addBody(body.name);

            SpawnHumanObjectives();
        }

        else
        {
            Request_ENV_Info();
        }

    }

    public void Update()
    {
        if (Humans != null && Ghosts != null)
            if (Humans.Count >= 1 && Ghosts.Count >= 1)
            {
                bool allHumansDead = true;
                foreach (var human in Humans)
                {
                    if (human.Value != null)
                    {
                        HumanControls temp = human.Value.gameObject.GetComponent<HumanControls>();
                        if (temp.isAlive)
                        {
                            allHumansDead = false;
                        }
                    }

                }

                if (allHumansDead)
                {
                    GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().resetGame(true);
                }
            }

    }

    public void spawnBody(GameObject body)
    {
        // CREATE THE BODY
        //find body spawns
        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("BodySpawn");

        //no body spawns
        if (System.Object.ReferenceEquals(null, spawnPoints))
        {
            Debug.LogError("No spawn points for BODY in level!");
            return;
        }

        //find random spawn point
        GameObject bodySpawn = spawnPoints[Random.Range(0, spawnPoints.Length)];

        //create body
        body.transform.position = bodySpawn.transform.position;
        body.transform.rotation = bodySpawn.transform.rotation;
        GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().body = body;

        return;
    }

    [RPC]
    public void SpawnFireplace(string fp)
    {
        GameObject[] fireplaceSpawnLocations = GameObject.FindGameObjectsWithTag("FireplaceSpawn");

        foreach (var fireplace in fireplaceSpawnLocations)
        {
            if (fireplace.name == fp)
                fireplace.GetComponent<Fireplace>().ToggleActive(true);
            else
                fireplace.GetComponent<Fireplace>().ToggleActive(false);
        }
    }

    void SpawnHumanObjectives()
    {
        //All human objectives have to have different names in the hierarchy!
        foreach (GameObject go in FireWood_ENV)
        {
            go.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
            SpawnWood(go);
        }
        foreach (GameObject go in FirePlaces_ENV)
        {
            go.transform.Find("FireplaceVolume").gameObject.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
            addFirePlace(go);
        }

        foreach (GameObject go in Gasoline_ENV)
        {
            go.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
            SpawnGas(go);
        }

        foreach (GameObject go in Matches_ENV)
        {
            go.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
            SpawnMatches(go);
        }
    }

    void SpawnWood(GameObject go)
    {
        if (woodSpawns.Count > 0)
        {
            int index = Random.Range(0, woodSpawns.Count - 1);
            GameObject woodSpawn = woodSpawns[index];
            woodSpawns.RemoveAt(index);
            go.transform.position = woodSpawn.transform.position;
            addFireWood(go);
        }

        else
            Debug.LogError("No more spawn points for Fire Wood");
    }

    void SpawnGas(GameObject go)
    {
        if (matchSpawns.Count > 0)
        {
            int index = Random.Range(0, gasSpawns.Count - 1);
            GameObject gasSpawn = gasSpawns[index];
            gasSpawns.RemoveAt(index);
            go.transform.position = gasSpawn.transform.position;
            addGasoline(go);
        }

        else
            Debug.LogError("No more spawn points for Gasoline");
    }

    void SpawnMatches(GameObject go)
    {
        if (matchSpawns.Count > 0)
        {
            int index = Random.Range(0, matchSpawns.Count - 1);
            GameObject matchSpawn = matchSpawns[index];
            matchSpawns.RemoveAt(index);
            go.transform.position = matchSpawn.transform.position;
            addMatches(go);
        }

        else
            Debug.LogError("No more spawn points for Matches");
    }

    public void Request_ENV_Info()
    {
        GetComponent<PhotonView>().RPC("RequestPull", PhotonTargets.MasterClient, null);
    }

    [RPC]
    private void RequestPull()
    {
        if (PhotonNetwork.isMasterClient)
        {
            GetComponent<PhotonView>().RPC("RetrievePull", PhotonTargets.Others,
                _HumanIDCount,
                _GhostIDCount,
                _BodyIDCount,
                _FireWoodIDCount,
                _FirePlaceIDCount,
                _MatchesIDCount,
                _GasolineIDCount);
        }
    }

    [RPC]
    private void RetrievePull(int h, int g, int b, int fw, int fp, int m, int gs)
    {
        _HumanIDCount = h;
        _GhostIDCount = g;
        _BodyIDCount = b;
        _FireWoodIDCount = fw;
        _FirePlaceIDCount = fp;
        _MatchesIDCount = m;
        _GasolineIDCount = gs;
    }
    public void OnLevelWasLoaded(int level)
    {
        GameObject.Find("_MENU_SYSTEM").transform.Find("GameOverMenu").gameObject.SetActive(false);
        Instantiate();
    }
    

    #endregion

    #region GetWithID

    public GameObject getHumanWithID(int id)
    {
        return Humans[id];
    }

    public GameObject getGhostWithID(int id)
    {
        return Ghosts[id];
    }

    public GameObject getBodyWithID(int id)
    {
        return Bodies[id];
    }

    public GameObject getFireWoodWithID(int id)
    {
        return FireWood[id];
    }

    public GameObject getFirePlaceWithID(int id)
    {
        return FirePlaces[id];
    }

    public GameObject getMatchesWithID(int id)
    {
        return Matches[id];
    }

    public GameObject getGasolineWithID(int id)
    {
        return Gasoline[id];
    }

    #endregion

    #region LocalAdd

    public int addHuman(string name)
    {
        GetComponent<PhotonView>().RPC("addHuman", PhotonTargets.AllBufferedViaServer, name, _HumanIDCount);
        return _HumanIDCount++;
    }

    public int addHuman(GameObject go)
    {
        GetComponent<PhotonView>().RPC("addHuman", PhotonTargets.AllBufferedViaServer, go.name, _HumanIDCount);
        return _HumanIDCount++;
    }

    public int addGhost(string name)
    {
        GetComponent<PhotonView>().RPC("addGhost", PhotonTargets.AllBufferedViaServer, name, _GhostIDCount);
        return _GhostIDCount++;
    }

    public int addGhost(GameObject go)
    {
        GetComponent<PhotonView>().RPC("addGhost", PhotonTargets.AllBufferedViaServer, go.name, _GhostIDCount);
        return _GhostIDCount++;
    }

    public int addBody(string name)
    {
        GetComponent<PhotonView>().RPC("addBody", PhotonTargets.AllBufferedViaServer, name, _BodyIDCount);
        return _BodyIDCount++;
    }

    public int addBody(GameObject go)
    {
        GetComponent<PhotonView>().RPC("addBody", PhotonTargets.AllBufferedViaServer, go.name, _BodyIDCount);
        return _BodyIDCount++;
    }

    public int addFireWood(string name)
    {
        GetComponent<PhotonView>().RPC("addFireWood", PhotonTargets.AllBufferedViaServer, name, _FireWoodIDCount);
        return _FireWoodIDCount++;
    }

    public int addFireWood(GameObject go)
    {
        GetComponent<PhotonView>().RPC("addFireWood", PhotonTargets.AllBufferedViaServer, go.name, _FireWoodIDCount);
        return _FireWoodIDCount++;
    }

    public int addFirePlace(string name)
    {
        GetComponent<PhotonView>().RPC("addFirePlace", PhotonTargets.AllBufferedViaServer, name, _FirePlaceIDCount);
        return _FirePlaceIDCount++;
    }

    public int addFirePlace(GameObject go)
    {
        GetComponent<PhotonView>().RPC("addFirePlace", PhotonTargets.AllBufferedViaServer, go.name, _FirePlaceIDCount);
        return _FirePlaceIDCount++;
    }

    public int addMatches(string name)
    {
        GetComponent<PhotonView>().RPC("addMatches", PhotonTargets.AllBufferedViaServer, name, _MatchesIDCount);
        return _MatchesIDCount++;
    }

    public int addMatches(GameObject go)
    {
        GetComponent<PhotonView>().RPC("addMatches", PhotonTargets.AllBufferedViaServer, go.name, _MatchesIDCount);
        return _MatchesIDCount++;
    }

    public int addGasoline(string name)
    {
        GetComponent<PhotonView>().RPC("addGasoline", PhotonTargets.AllBufferedViaServer, name, _GasolineIDCount);
        return _GasolineIDCount++;
    }

    public int addGasoline(GameObject go)
    {
        GetComponent<PhotonView>().RPC("addGasoline", PhotonTargets.AllBufferedViaServer, go.name, _GasolineIDCount);
        return _GasolineIDCount++;
    }
    #endregion

    #region NetworkAdd
    [RPC]
    private void addHuman(string name, int id)
    {
        if (_HumanIDCount <= id)
            _HumanIDCount = id + 1;
        GameObject go = GameObject.Find(name).gameObject;
        go.GetComponent<GameId>().ID = id;
        go.name += id.ToString();
        Humans.Add(id, go);
    }

    [RPC]
    private void addGhost(string name, int id)
    {
        if (_GhostIDCount <= id)
            _GhostIDCount = id + 1;
        GameObject go = GameObject.Find(name).gameObject;
        go.GetComponent<GameId>().ID = id;
        go.name += id.ToString();
        Ghosts.Add(id, go);
    }

    [RPC]
    private void addBody(string name, int id)
    {
        if (_BodyIDCount <= id)
            _BodyIDCount = id + 1;
        GameObject go = GameObject.Find(name).gameObject;
        go.GetComponent<GameId>().ID = id;
        go.name += id.ToString();
        Bodies.Add(id, go);
    }

    [RPC]
    private void addFireWood(string name, int id)
    {
        if (_FireWoodIDCount <= id)
            _FireWoodIDCount = id + 1;
        GameObject go = GameObject.Find(name).gameObject;
        go.GetComponent<GameId>().ID = id;
        go.name += id.ToString();
        FireWood.Add(id, go);
    }

    [RPC]
    private void addFirePlace(string name, int id)
    {
        if (_FirePlaceIDCount <= id)
            _FirePlaceIDCount = id + 1;
        GameObject go = GameObject.Find(name).gameObject;
        GameId gid = go.GetComponent<GameId>();
        gid.ID = id;
        go.name += id.ToString();
        FirePlaces.Add(id, go);
    }

    [RPC]
    private void addMatches(string name, int id)
    {
        if (_MatchesIDCount <= id)
            _MatchesIDCount = id + 1;
        GameObject go = GameObject.Find(name).gameObject;
        go.GetComponent<GameId>().ID = id;
        go.name += id.ToString();
        Matches.Add(id, go);
    }

    [RPC]
    private void addGasoline(string name, int id)
    {
        if (_GasolineIDCount <= id)
            _GasolineIDCount = id + 1;
        GameObject go = GameObject.Find(name).gameObject;
        go.GetComponent<GameId>().ID = id;
        go.name += id.ToString();
        Gasoline.Add(id, go);
    }

    #endregion

    #region Remove
    [RPC]
    public bool removeHuman(int id)
    {
        if (System.Object.ReferenceEquals(Humans[id], null))
            return false;
        else
        {
            Humans.Remove(id);
            return true;
        }
    }

    [RPC]
    public bool removeGhost(int id)
    {
        if (System.Object.ReferenceEquals(Ghosts[id], null))
            return false;
        else
        {
            Ghosts.Remove(id);
            return true;
        }
    }

    [RPC]
    public bool removeBody(int id)
    {
        if (System.Object.ReferenceEquals(Bodies[id], null))
            return false;
        else
        {
            Bodies.Remove(id);
            return true;
        }
    }

    [RPC]
    public bool removeFireWood(int id)
    {
        if (System.Object.ReferenceEquals(FireWood[id], null))
            return false;
        else
        {
            FireWood.Remove(id);
            return true;
        }
    }

    [RPC]
    public bool removeMatches(int id)
    {
        if (System.Object.ReferenceEquals(Matches[id], null))
            return false;
        else
        {
            Matches.Remove(id);
            return true;
        }
    }

    [RPC]
    public bool removeGasoline(int id)
    {
        if (System.Object.ReferenceEquals(Gasoline[id], null))
            return false;
        else
        {
            Gasoline.Remove(id);
            return true;
        }
    }

    #endregion

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){}
}
