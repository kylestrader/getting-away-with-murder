﻿using UnityEngine;
using System.Collections;

public class GhostFootsteps : MonoBehaviour 
{
	public AudioClip [] stepsWood;
	public AudioClip[] landWood;
	
	public float walkAudioSpeed = 2.0f;
	private AudioClip walk;	
	private float walkAudioTimer = 0.0f;	
	public float timeActive;
	private float maxTimeActive = 2.0f;
	public float cooldownTime;
	private float cooldownTimeMax = 5.0f;
	bool isActive = false;
	
	void Start()
	{
	}
	
	void Update()
	{
		DecideIfActive ();
	 	if (isActive) PlayFootsteps();
	}

	void ToggleStuff()
	{
		if (isActive)
		{
			GetComponent<AudioSource>().enabled = true;
			GetComponent<SpriteRenderer>().enabled = true;
			GetComponentInChildren<ParticleSystem>().Play();
		}
		else
		{
			GetComponent<AudioSource>().enabled = false;
			GetComponent<SpriteRenderer>().enabled = false;
			GetComponentInChildren<ParticleSystem>().Stop();
		}
	}

	void DecideIfActive()
	{
		if (!isActive)
		{
			if (cooldownTime <= 0)
			{
				int r = Random.Range(0, 100);

				if (r > 49)
				{
					isActive = true;
					ToggleStuff();
				}
				else cooldownTime = cooldownTimeMax;
			}
			else
			{
				cooldownTime -= Time.deltaTime;
			}
		}
		else
		{
			timeActive += Time.deltaTime;

			if (timeActive >= maxTimeActive)
			{
				timeActive = 0;
				isActive = false;
				ToggleStuff();
			}
		}
	}
	
	void PlayFootsteps() 
	{
		walkAudioTimer += Time.deltaTime;
		if ( walkAudioTimer > walkAudioSpeed )
		{
			GetComponent<AudioSource>().clip = stepsWood[Random.Range(0, stepsWood.Length)];
			GetComponent<AudioSource>().Play();
			walkAudioTimer = 0.0f;
		}
	}
	
}
