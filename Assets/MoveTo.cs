﻿using UnityEngine;
using System.Collections;

public class MoveTo : MonoBehaviour {

    public Transform target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, target.position, 0.7f * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, 0.7f * Time.deltaTime);
	}

    public void setTarget(string go)
    {
        GameObject tmpGo = GameObject.Find(go);
        if (!System.Object.ReferenceEquals(tmpGo, null))
        {
            target = tmpGo.transform;
        }
    }
}
