﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[AddComponentMenu("Camera-Control/Mouse Look")]
public class HumanControls : Photon.MonoBehaviour {

	//Player owned Objects
	public GameObject body;
    private Camera humanCamera;
    private Camera humanMapCamera;
	public CharacterMotor chMot;
    public GameObject MiniMap;

	//Player stats
	public float sprintBoost = 1.4f;
	public float sprintTime = 5f;
	private float timeSpentSprinting = 0f;
	public int hp = 100;
	private int maxHp;
	public bool isDead = false;

	public int mirrorDmg;

    public int numVials;
    public int maxNumVials;

    private float mirrorComfortDamagePerSecond;

	private int hpRegen;

	private float throwForce;

	public const float distanceBeforeScared = 40f;
	public float maxComfortLostPerSecond;
	public bool isHolding = false;
	public bool holdingWood = false;

	public float maxForwardSpeed = 0.0f;
	public float maxSidewaysSpeed = 0.0f;
	public float maxBackwardsSpeed = 0.0f;

	public bool isSlowed = false;

	public float minForwardSpeed = 0.0f;
	public float minSidewaysSpeed = 0.0f;
	public float minBackwardsSpeed = 0.0f;

	public float maxBaseJumpHeight = 0.0f;
	public float minBaseJumpHeight = 0.0f;

	public float maxWalkAudioSpeed = 0.0f;
	public float minWalkAudioSpeed = 0.0f;
	public float maxHeadBobSpeed = 0.0f;
	public float minHeadBobSpeed = 0.0f;

	//FOV mod logic
	public const float fovShutter = 20;
	public const float fov = 90;
	public const float fovLerpStep = 0.12f;

	//Corpse interaction logic
	public const float bodyHoldRotation = -30f; //rotates on x-axis

	//Human UI elements
	public GameObject humanUI;
	public Text hpUiText;
	public Text woodUIText;
	public Text matchUIText;
	public Text tinderUIText;
	public Image vial1;
	public Image vial2;
	public Image vial3;
	public GameObject staminaUiSlider;

	//Player mouse information
	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	float lookX_Amt = 0.0f;
	float lookY_Amt = 0.0f;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationY = 0F;

	//cooldown after EMP
	public float lanternTimerMax = 1.5f;
	public float lanternTimer = 0.0f;
	public bool lanternOnCD = false;

	//Human Sees Ghost
	public AudioClip[] seenGhost;
	public AudioClip strikeMatch;
	public AudioClip completedObjective;
	public AudioClip[] deathScreams;
	bool soundGhostPlaying = false;

	public AudioClip[] woodCalls;
	public AudioClip[] matchCalls;
	public AudioClip[] gasCalls;
	public AudioClip[] bodyCalls;
	public AudioClip[] ghostCalls;
	public AudioClip[] idleCalls;

    public float interactionRange = 2.0f;
    public float furnitureDamage = 10.0f;

    public GameObject SpectatorPrefab;
    public bool isAlive = true;
	public struct Inventory
	{
		public bool hasWood;
		public bool hasMatches;
		public bool hasTinder;
	}
	public Inventory inventory;

	public GameObject ghostBurn;

	// Use this for initialization
	void Start ()
	{
        if (!GetComponent<PhotonView>().isMine)
        {
            this.GetComponent<Camera>().GetComponent<AudioListener>().enabled = false;
            this.GetComponent<Camera>().enabled = false;
        }
		//GameObject.Find ("_SCRIPTS").GetComponent<PhotonView> ().RPC ("PickFireplaceSpawn", PhotonTargets.All, null);
		SetupHP();
		SetupMovement ();
		GetVariableDefaults ();
        isAlive = true;
		Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

		GetComponent<AudioSource>().volume = 0.25f;
	}
	
	// Update is called once per frame
	void Update () {
		HandleCorpseDepictedMovement ();
		HandleInput ();
		ChangeUI ();
		CheckForMirror ();
		CheckGhost ();
		LanternCooldownUpdate ();
		HoldingBodyMovement ();
		HpRegen ();

		if (hp <= 0 && !isDead)
		{
            FadeToBlack();
			HumanDies();
		}

        if (PhotonNetwork.connectionState == ConnectionState.Disconnecting)
        {
            HumanDies();
        }
	}

	public void PlayObjectiveSound()
	{
		GetComponent<AudioSource>().volume = 0.5f;
		GetComponent<AudioSource> ().clip = completedObjective;
		GetComponent<AudioSource> ().Play ();
	}

	public void PlayMatchSound()
	{
		GetComponent<AudioSource> ().clip = strikeMatch;
		GetComponent<AudioSource> ().Play ();
	}

	private void StartLanternCooldown()
	{
		lanternTimer = lanternTimerMax;
		lanternOnCD = true;
	}

	private void LanternCooldownUpdate()
	{
		if (lanternOnCD)
		{
			lanternTimer -= Time.deltaTime;
			if (lanternTimer < 0)
			{
				lanternTimer = 0;
				lanternOnCD = false;
			}
		}
	}

	public void PlayWoodCall()
	{
		GetComponent<AudioSource>().volume = 1.0f;
		GetComponent<AudioSource> ().clip = woodCalls [Random.Range (0, woodCalls.Length)];
		GetComponent<AudioSource> ().Play ();
	}

	public void PlayMatchCall()
	{
		GetComponent<AudioSource>().volume = 1.0f;
		GetComponent<AudioSource> ().clip = matchCalls [Random.Range (0, matchCalls.Length)];
		GetComponent<AudioSource> ().Play ();
	}

	public void PlayGasCall()
	{
		GetComponent<AudioSource>().volume = 1.0f;
		GetComponent<AudioSource> ().clip = gasCalls [Random.Range (0, gasCalls.Length)];
		GetComponent<AudioSource> ().Play ();
	}

	public void PlayIdleCall()
	{
		GetComponent<AudioSource>().volume = 1.0f;
		GetComponent<AudioSource> ().clip = idleCalls [Random.Range (0, idleCalls.Length)];
		GetComponent<AudioSource> ().Play ();
	}

	public void PlayBodyCall()
	{
		GetComponent<AudioSource>().volume = 1.0f;
		GetComponent<AudioSource> ().clip = bodyCalls [Random.Range (0, bodyCalls.Length)];
		GetComponent<AudioSource> ().Play ();
	}

	private void HpRegen()
	{
		if (hp < maxHp)
		{
			hp += hpRegen;
		}

		if (hp > maxHp) hp = maxHp;
	}

	private void GetVariableDefaults()
	{
		throwForce = GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().BodyThrowForce;
		maxComfortLostPerSecond = GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().VisibilityDmg;
		lanternTimerMax = GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().LanternReactivateCD;
        maxNumVials = GameObject.Find("_SCRIPTS").GetComponent<Variables>().MaxNumVials;
		mirrorDmg = GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().MirrorDmg;

		inventory.hasWood = false;
		inventory.hasTinder = false;
		inventory.hasMatches = false;
	}

	public delegate void AudioCallback();

	void AudioFinished()
	{
        //Only deactive the dead one
        if(GetComponent<PhotonView>().isMine)
        {
            isAlive = false;
            Debug.Log("Sound ended!");
            GameObject spectator = (GameObject)Instantiate(SpectatorPrefab, this.transform.position + Vector3.up * 2, this.transform.rotation);
            humanUI.SetActive(false);
            gameObject.SetActive(false);

            GetComponent<PhotonView>().RPC("destroyHuman", PhotonTargets.AllBufferedViaServer);
        }

	}

	void PlaySoundWithCallback(AudioClip clip, AudioCallback callback)
	{
		GetComponent<AudioSource> ().clip = clip;
		GetComponent<AudioSource> ().Play ();
		StartCoroutine (DelayedCallback (clip.length, callback));
        
	}

    private void FadeToBlack()
    {
        Debug.Log(this.GetComponentInChildren<Light>().color);
        this.GetComponentInChildren<Light>().color = Color.Lerp(this.GetComponentInChildren<Light>().color, Color.black, Time.deltaTime * 10);

    }

	private IEnumerator DelayedCallback(float time, AudioCallback callback)
	{
		yield return new WaitForSeconds (time);
		callback ();
	}

	void HumanDies()
	{
		Debug.Log ("Ghost won!");
		isDead = true;
		GetComponent<Footsteps> ().enabled = false;
		GetComponent<CharacterMotor> ().canControl = false;
        humanUI.transform.Find("DeathFade").GetComponent<FadeToBlack>().enabled = true;
		
        this.GetComponent<PhotonView>().RPC("networkedPlaySoundWithCallback", PhotonTargets.AllBufferedViaServer, null);
		//GameObject.Find ("_NETWORKED_SCRIPTS").GetComponent<RoundManager> ().resetGame (true);
	}

    private void DropItems()
    {
        if (inventory.hasMatches)
            GameObject.Find("_GAME_ENV_MANAGER").GetComponent<GameEnvironment>().addMatches(PhotonNetwork.Instantiate("MatchBox", transform.position + Vector3.up, Quaternion.identity, 0));
        if (inventory.hasTinder)
            GameObject.Find("_GAME_ENV_MANAGER").GetComponent<GameEnvironment>().addGasoline(PhotonNetwork.Instantiate("Tinder", transform.position + Vector3.up, Quaternion.identity, 0));
        if (inventory.hasWood)
            DropWood();
        if (body)
            DropBody();
    }

	void CheckGhost(){
		GameObject ghost = GameObject.FindGameObjectWithTag("Ghost");
        if (!System.Object.ReferenceEquals(null,ghost))
        {
            if (!ghost.GetComponent<GhostControls>().isInvisible)
		    {	
		    	Vector3 ghostVec = ghost.transform.position - transform.position;
		    	RaycastHit hit;
		    	if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), ghostVec.normalized, out hit, distanceBeforeScared))
		    	{
		    		
		    		if (hit.collider.gameObject.tag == "Ghost")
		    		{
		    			if (!soundGhostPlaying)
		    			//if (!GetComponent<AudioSource>().isPlaying)
		    			{
		    				GetComponent<AudioSource>().Stop();
		    				GetComponent<AudioSource>().clip = seenGhost[Random.Range(0, seenGhost.Length)];
		    				GetComponent<AudioSource>().Play();
		    				soundGhostPlaying = true;
		    			}

		    			if (!GetComponent<AudioSource>().isPlaying)
		    			{
		    				soundGhostPlaying = false;
		    			}
		    			Vector3 lookAtVec = humanCamera.transform.forward;
		    			float ghostVecMag = Vector3.Magnitude (ghostVec);
		    			float lookAtMag = Vector3.Magnitude (lookAtVec);
		    			float radians = Mathf.Acos (Vector3.Dot (ghostVec, lookAtVec) / ghostVecMag * lookAtMag);
		    			float angle = radians * 180 / Mathf.PI; 
		    			float normalFOV = fov / 2f;

		    			if (Mathf.Abs (angle) <= normalFOV) 
		    			{

                            float calculatedTick = ((maxComfortLostPerSecond * Time.deltaTime) - ((maxComfortLostPerSecond * Time.deltaTime) * (Mathf.Abs(angle) / normalFOV)));
		    				float calculatedFOV = fov + ((fovShutter * (1f - (Mathf.Abs(angle) / normalFOV))) * (1f - (hit.distance / distanceBeforeScared)));
		    				humanCamera.fieldOfView = humanCamera.fieldOfView + (calculatedFOV - humanCamera.fieldOfView) * (fovLerpStep * (humanCamera.fieldOfView / calculatedFOV));
		    				Debug.Log (fov + (fovShutter * (1f - (Mathf.Abs(angle) / normalFOV))));
		    				calculatedTick *= (1f - (hit.distance / distanceBeforeScared));
		    				hp -= (int)calculatedTick;
                            
		    			}
		    			else humanCamera.fieldOfView = humanCamera.fieldOfView + (fov - humanCamera.fieldOfView) * (fovLerpStep * (humanCamera.fieldOfView / fov));
		    		}
		    		else humanCamera.fieldOfView = humanCamera.fieldOfView + (fov - humanCamera.fieldOfView) * (fovLerpStep * (humanCamera.fieldOfView / fov));
		    	}
		    }
		    else humanCamera.fieldOfView = humanCamera.fieldOfView + (fov - humanCamera.fieldOfView) * (fovLerpStep * (humanCamera.fieldOfView / fov));
        }
        else humanCamera.fieldOfView = humanCamera.fieldOfView + (fov - humanCamera.fieldOfView) * (fovLerpStep * (humanCamera.fieldOfView / fov));

        if (humanCamera.fieldOfView != humanMapCamera.fieldOfView)
        {
            humanMapCamera.fieldOfView = humanCamera.fieldOfView;
        }
	}

	void CheckForMirror()
	{
		RaycastHit hit;


		// right click, so send out a ray to pick up object
		if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 200, 9))
		{
			if (hit.collider.gameObject.tag == "Mirror")
			{
                Debug.Log("MIRROR");
				if (isHolding){
                    hp -= (int)mirrorComfortDamagePerSecond * (int)Time.deltaTime;
					Debug.Log("Player looking at mirror!");
				}
			}
		}
	}

	private void SetupHP()
	{
		maxHp = (int)GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().MaxHealth;
		hp = maxHp;
		hpRegen = (int)GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().HealthRegen;

		humanUI = GameObject.Find ("HumanUI(Clone)");
		hpUiText = GameObject.Find ("HumanUI(Clone)/HealthBar/Text").GetComponent<Text>();

		woodUIText = GameObject.Find ("HumanUI(Clone)/WoodInv").GetComponent<Text>();
		matchUIText = GameObject.Find ("HumanUI(Clone)/MatchInv").GetComponent<Text>();
		tinderUIText = GameObject.Find ("HumanUI(Clone)/TinderInv").GetComponent<Text>();
		hpUiText.GetComponent<Text> ().text = "HP: " + maxHp.ToString();
		woodUIText.GetComponent<Text> ().text = "No Wood";
		woodUIText.GetComponent<Text> ().color = Color.red;
		matchUIText.GetComponent<Text> ().text = "No Matches";
		matchUIText.GetComponent<Text> ().color = Color.red;
		tinderUIText.GetComponent<Text> ().text = "No Tinder";
		tinderUIText.GetComponent<Text> ().color = Color.red;

        numVials = 3;
		vial1 = GameObject.Find ("HumanUI(Clone)/Vial1").GetComponent<Image> ();
		vial2 = GameObject.Find ("HumanUI(Clone)/Vial2").GetComponent<Image> ();
		vial3 = GameObject.Find ("HumanUI(Clone)/Vial3").GetComponent<Image> ();

		staminaUiSlider = GameObject.Find ("HumanUI(Clone)/StamBar/StaminaSlider");
		staminaUiSlider.GetComponent<Slider> ().value = (100f * ((sprintTime - timeSpentSprinting) / sprintTime));
	}

	private void SetupMovement()
	{
		chMot = GetComponent<CharacterMotor>();
		
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;

        humanCamera = transform.Find("Main Camera").GetComponent<Camera>();
        humanMapCamera = transform.Find("Map Camera").GetComponent<Camera>();
		
		//stats initialization
		maxForwardSpeed = chMot.movement.maxForwardSpeed;
		maxSidewaysSpeed = chMot.movement.maxSidewaysSpeed;
		maxBackwardsSpeed = chMot.movement.maxBackwardsSpeed;
		
		minForwardSpeed = chMot.movement.maxForwardSpeed * 0.7f;
		minSidewaysSpeed = chMot.movement.maxSidewaysSpeed * 0.7f;
		minBackwardsSpeed = chMot.movement.maxBackwardsSpeed * 0.7f;
		
		maxBaseJumpHeight = chMot.jumping.baseHeight;
		minBaseJumpHeight = chMot.jumping.baseHeight * 0.5f;
		
		maxWalkAudioSpeed = GetComponent<Footsteps>().walkAudioSpeed;
		minWalkAudioSpeed = GetComponent<Footsteps>().walkAudioSpeed * 1.3f;
		maxHeadBobSpeed = GetComponent<HeadBobber>().bobbingSpeed;
		minHeadBobSpeed = GetComponent<HeadBobber>().bobbingSpeed * 0.7f;
	}

	void SetVialUI()
	{
		if (numVials == 0)
		{
			vial1.enabled = false;
			vial2.enabled = false;
			vial3.enabled = false;
		}
		else if (numVials == 1)
		{
			vial1.enabled = true;
			vial2.enabled = false;
			vial3.enabled = false;
		}
		else if (numVials == 2)
		{
			vial1.enabled = true;
			vial2.enabled = true;
			vial3.enabled = false;
		}
		else if (numVials == 3)
		{
			vial1.enabled = true;
			vial2.enabled = true;
			vial3.enabled = true;
		}
	}

	void ChangeUI()
	{
		hpUiText.GetComponent<Text>().text = "HP: " + hp.ToString();
		staminaUiSlider.GetComponent<Slider> ().value = (100f * ((sprintTime - timeSpentSprinting) / sprintTime));

		if (inventory.hasWood)
		{
			woodUIText.GetComponent<Text> ().color = Color.green;
			woodUIText.GetComponent<Text> ().text = "Have Wood";
		}
		else
		{
			woodUIText.GetComponent<Text> ().color = Color.red;
			woodUIText.GetComponent<Text> ().text = "No Wood";
		}

		if (inventory.hasMatches)
		{
			matchUIText.GetComponent<Text> ().color = Color.green;
			matchUIText.GetComponent<Text> ().text = "Have Matches";
		}
		else
		{
			matchUIText.GetComponent<Text> ().color = Color.red;
			matchUIText.GetComponent<Text> ().text = "No Matches";
		}

		if (inventory.hasTinder)
		{
			tinderUIText.GetComponent<Text> ().color = Color.green;
			tinderUIText.GetComponent<Text> ().text = "Have Gas";
		}
		else
		{
			tinderUIText.GetComponent<Text> ().color = Color.red;
			tinderUIText.GetComponent<Text> ().text = "No Gas";
		}

		SetVialUI ();

		if (GameObject.Find("OptionsMenu") != null)
		{
			sensitivityX = GameObject.Find("OptionsMenu/XSensitivity").GetComponent<Slider>().value;
			sensitivityY = GameObject.Find("OptionsMenu/YSensitivity").GetComponent<Slider>().value;
		}
	}

	void HandleCorpseDepictedMovement()
	{	
        if(GetComponent<PhotonView>().isMine)
		if (isHolding || isSlowed)
		{
			//modify movement
			chMot.movement.maxForwardSpeed = minForwardSpeed;
			chMot.movement.maxSidewaysSpeed = minSidewaysSpeed;
			chMot.movement.maxBackwardsSpeed = minBackwardsSpeed;
			chMot.jumping.baseHeight = minBaseJumpHeight;
		}
		else
		{
			//modify movement
			chMot.movement.maxForwardSpeed = maxForwardSpeed;
			chMot.movement.maxSidewaysSpeed = maxSidewaysSpeed;
			chMot.movement.maxBackwardsSpeed = maxBackwardsSpeed;
			chMot.jumping.baseHeight = maxBaseJumpHeight;
		}
	}
	
	void HandleInput()
	{
		//menu stuff
		GameObject optionsMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("OptionsMenu");
		GameObject pauseMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("PauseMenu");

		//Mouse
		float dx = Input.GetAxis("Mouse X");
		dx = dx * Mathf.Abs(dx);
		dx = Mathf.Clamp(dx, -.5f, .5f);
		float dy = Input.GetAxis("Mouse Y");
		dy = dy * Mathf.Abs(dy);
		dy = Mathf.Clamp(dy, -.5f, .5f);

		lookX_Amt = dx/2; // Added to corpse target position relative to player
		lookY_Amt = dy/2; 

		if (axes == RotationAxes.MouseXAndY)
		{
			float rotationX = transform.localEulerAngles.y + dx * sensitivityX;
			rotationY += dy * sensitivityY;
			rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
            humanCamera.transform.localEulerAngles = new Vector3(-rotationY, 0, 0);
            humanMapCamera.transform.localEulerAngles = new Vector3(-rotationY, 0, 0);
			transform.localEulerAngles = new Vector3(0, rotationX, 0);
		}
		else if (axes == RotationAxes.MouseX)
		{
			transform.Rotate(0, dx * sensitivityX, 0);
		}
		else
		{
			rotationY += dy * sensitivityY;
			rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
			humanCamera.transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}

		//Keyboard
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (!pauseMenu.GetActive() && !optionsMenu.GetActive())
			{
				pauseMenu.SetActive(true);
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
			else if (pauseMenu.GetActive())
			{
				pauseMenu.SetActive(false);
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			else if (optionsMenu.GetActive())
			{
				optionsMenu.SetActive(false);
				pauseMenu.SetActive(true);
			}
		}

		
		if (Input.GetKeyDown(KeyCode.F))
		{
			if (!lanternOnCD)
			{
				if (transform.Find("Flashlight").GetComponent<Light>().enabled)
					GetComponent<PhotonView>().RPC("turnOffLight", PhotonTargets.AllBufferedViaServer, null);
				else
				{
					GetComponent<PhotonView>().RPC("turnOnLight", PhotonTargets.AllBufferedViaServer, null);
					PlayMatchSound();
				}
			}
		}

		//Sprinting
		if (Input.GetKey(KeyCode.LeftShift)) 
		{
			if (timeSpentSprinting < sprintTime)
			{
				timeSpentSprinting += Time.deltaTime;
				chMot.movement.maxForwardSpeed = chMot.movement.maxForwardSpeed * sprintBoost;
			}

			else if (timeSpentSprinting > sprintTime)
			{
				timeSpentSprinting = sprintTime;
			}
		} 
		else 
		{
			if (timeSpentSprinting > 0)
			{
				timeSpentSprinting -= Time.deltaTime;
			}

			else if (timeSpentSprinting < 0)
			{
				timeSpentSprinting = 0;
			}
		}
		//Debug.Log (GameObject.Find ("HumanUI(Clone)/StamBar/StaminaSlider").GetComponent<Slider> ().maxValue);

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (MiniMap.GetComponent<MapRotate>().isActive)
            {
                MiniMap.GetComponent<MapRotate>().PutAway();
            }
            else MiniMap.GetComponent<MapRotate>().TakeOut();
        }

		if (Input.GetMouseButtonDown(1))
		{
			if (!isHolding && !holdingWood)
			{
				RaycastHit outpt;
				// right click, so send out a ray to pick up object
				if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out outpt))
				{
					if (outpt.collider.gameObject.tag == "Body")
					{
						PickupBody(outpt);
					}
					else if (outpt.collider.gameObject.tag == "Firewood")
					{
						PickupWood(outpt);
					}
				}
			}
			else
			{	
			    if (!holdingWood) DropBody ();
				else DropWood ();
			}
		}
		if (Input.GetMouseButtonDown(0))
		{
            RaycastHit outpt;
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out outpt))
            {
                if (outpt.collider.gameObject.tag == "Body")
                {
                    GetComponent<HumanPing>().SendPing(outpt.collider.GetComponent<GameId>().ID, HumanPing.WayPointType.WP_BODY);
                }
                else if (outpt.collider.gameObject.tag == "Firewood")
                {
                    GetComponent<HumanPing>().SendPing(outpt.collider.GetComponent<GameId>().ID, HumanPing.WayPointType.WP_LOG);
                }
                else if (outpt.collider.gameObject.tag == "Matches")
                {
                    GetComponent<HumanPing>().SendPing(outpt.collider.GetComponent<GameId>().ID, HumanPing.WayPointType.WP_MATCHES);
                }
                else if (outpt.collider.gameObject.tag == "Gasoline")
                {
                    GetComponent<HumanPing>().SendPing(outpt.collider.GetComponent<GameId>().ID, HumanPing.WayPointType.WP_GAS);
                }
				else 
				{
					GetComponent<HumanPing>().SendPing(GetComponent<GameId>().ID, 0);
				}
            }

            //GetComponent<HumanPing>().SendPing(GetComponent<GameId>().ID, 0);
		}

		if (Input.GetKeyDown(KeyCode.Q))
		{
			if (numVials > 0) ThrowHolyWater();
		}
        if(Input.GetKeyDown(KeyCode.P))
        {
            //pushBack(-transform.forward,100,5);
			HumansWin(transform);
        }

        //Interact
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * interactionRange, Color.red, 2);
			if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, interactionRange, ~(1 <<13)))
            {
                Debug.Log(hit.collider.gameObject.tag);
                if (hit.collider.gameObject.tag == "Interactable")
                {
                    Debug.Log("Interact Called");
                    hit.collider.gameObject.GetComponentInChildren<Interactable>().Interact(false);
                }
                    //Need to tell other clients that you picked up the item so that they know to drop it if you leave.
				else if (hit.collider.gameObject.tag == "Matches" 
                    || hit.collider.gameObject.tag == "Firewood"
                    || hit.collider.gameObject.tag == "Gasoline")
				{
					Debug.Log("PickUp Called");
					hit.collider.gameObject.GetComponentInChildren<Item>().PickUp(this);
				}
				else if (hit.collider.gameObject.tag == "Fireplace")
				{
					Debug.Log("Interacted with Fireplace");
					hit.collider.gameObject.GetComponent<Fireplace>().HumanInteract(this);

					if (isHolding && hit.collider.gameObject.GetComponent<Fireplace>().IsActive())
					{
                        HumansWin(hit.collider.transform);
					}
				}
			}
		}
	}

	void HumansWin(Transform fp)
	{
		if (body != null)
		{
            body.transform.Find("FireComplex").gameObject.SetActive(true);
			GameObject ghost = GameObject.FindGameObjectWithTag("Ghost");
			GhostBurn ();

			humanUI.transform.Find("DeathFade").GetComponent<FadeToBlack>().enabled = true;
			FadeToBlack ();
			if (ghost != null) ghost.GetComponent<GhostControls> ().Die ();

			GameObject.Find("_GLOBAL_AMBIENT_SPEAKER").GetComponent<AudioSource>().loop = false;
			GameObject.Find("_GLOBAL_AMBIENT_SPEAKER").GetComponent<SoundBank>().Play("MurderersWins");
			GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().resetGame(false);
		}
	}

	void GhostBurn()
	{
		GameObject burn = (GameObject)Instantiate (ghostBurn);
		burn.transform.position = this.transform.position + (transform.forward / 3) - (transform.up / 8);
		burn.transform.LookAt (gameObject.transform);
	}

    public void pushBack(Vector3 vectorToBePushedBack, float pushBackForce, float damage)
    {
        this.GetComponent<PhotonView>().RPC("networkPushBack", PhotonTargets.AllBufferedViaServer, vectorToBePushedBack, pushBackForce, damage);
    }

	private void ThrowHolyWater()
	{
		GameObject proj = (GameObject)Instantiate(Resources.Load ("HolyWaterProj"));
		proj.transform.position = Camera.main.transform.position + (Camera.main.transform.forward);
		proj.GetComponent<Rigidbody> ().AddForce (Camera.main.transform.forward * throwForce);
        numVials--;
        Debug.Log(numVials);
	}

	void OnParticleCollision(GameObject other)
	{
		//hp -= mirrorDmg;
	}
	

	private void HoldingBodyMovement()
	{
		if ((isHolding || holdingWood) || isSlowed)
		{	
			GetComponent<Footsteps>().walkAudioSpeed = minWalkAudioSpeed;
			GetComponent<HeadBobber>().bobbingSpeed = minHeadBobSpeed;
			
			if (Vector3.Distance(transform.position, body.transform.position) <= 5f)
			{
				if (isHolding)
				{
					//Tell corpse to be held near the players head within the FOV
					body.gameObject.GetComponent<BodyControl>().setPointToFloatTowards(
						humanCamera.transform.position + humanCamera.transform.TransformDirection(
						new Vector3(1.1f + lookX_Amt, 0.3f + lookY_Amt, 0.4f))); //This Vector 3 depicts where the corpse will be held in relation to the player
					
					//Tell the corpse how it should be rotated
					body.gameObject.GetComponent<BodyControl>().setRotationToLookTowards(
						Quaternion.Euler(humanCamera.transform.rotation.eulerAngles.x + bodyHoldRotation,
									 humanCamera.transform.rotation.eulerAngles.y,
									 humanCamera.transform.rotation.eulerAngles.z));
				}
				else if (holdingWood)
				{
					//Tell corpse to be held near the players head within the FOV
					body.gameObject.GetComponent<FirewoodScript>().setPointToFloatTowards(
						humanCamera.transform.position + humanCamera.transform.TransformDirection(
						new Vector3(1.1f + lookX_Amt, 0.3f + lookY_Amt, 0.4f))); //This Vector 3 depicts where the corpse will be held in relation to the player
					
					//Tell the corpse how it should be rotated
					body.gameObject.GetComponent<FirewoodScript>().setRotationToLookTowards(
						Quaternion.Euler(humanCamera.transform.rotation.eulerAngles.x + bodyHoldRotation,
					                 humanCamera.transform.rotation.eulerAngles.y,
					                 humanCamera.transform.rotation.eulerAngles.z));
				}
			}
			
			else
			{
				//drop body if it is dragged too far away from the player
				Physics.IgnoreCollision(body.GetComponent<Collider>(), GetComponent<Collider>(), false);
				body.GetComponent<Rigidbody>().useGravity = true;
				body.GetComponent<Rigidbody>().isKinematic = false;
				body.GetComponent<BodyControl>().isHeld = false;
				body.transform.parent = null;
				body.transform.GetComponent<Rigidbody>().drag = 0f;
				isHolding = false;
			}
		}
		else
		{
			GetComponent<Footsteps>().walkAudioSpeed = maxWalkAudioSpeed;
			GetComponent<HeadBobber>().bobbingSpeed = maxHeadBobSpeed;
		}
	}

	#region BodyInteraction
	private void PickupBody(RaycastHit outpt)
	{
		if (!isHolding && !holdingWood){
			body = outpt.collider.gameObject;
            if (body)
            {
                body.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
                GetComponent<PhotonView>().RPC("networkHandleBodyPickup", PhotonTargets.OthersBuffered, body.name);
				Physics.IgnoreCollision(body.GetComponent<Collider>(), GetComponent<Collider>(), true);
				body.GetComponent<Collider>().isTrigger = true;	
				body.GetComponent<Rigidbody>().useGravity = false;
				body.GetComponent<Rigidbody>().isKinematic = true;
				body.GetComponent<BodyControl>().isHeld = true;
                body.layer = 20;
			}
			body.transform.parent = Camera.main.transform;
			body.transform.GetComponent<Rigidbody>().drag = 5f;
            body.GetComponent<NetworkInterpolation>().enabled = true;
			isHolding = true;
		}
	}

	private void PickupWood(RaycastHit outpt)
	{
		if (!isHolding && !holdingWood){
			body = outpt.collider.gameObject;
			if (body)
			{
				body.GetPhotonView().TransferOwnership(PhotonNetwork.player.ID);
				GetComponent<PhotonView>().RPC("networkHandleWoodPickup", PhotonTargets.OthersBuffered, body.name);
				Physics.IgnoreCollision(body.GetComponent<Collider>(), GetComponent<Collider>(), true);
				body.GetComponent<Collider>().isTrigger = true;	
				body.GetComponent<Rigidbody>().useGravity = false;
				body.GetComponent<Rigidbody>().isKinematic = true;
                body.GetComponent<FirewoodScript>().isHeld = true;
                body.layer = 20;
			}
			body.transform.parent = Camera.main.transform;
			body.transform.GetComponent<Rigidbody>().drag = 5f;
			body.GetComponent<NetworkInterpolation>().enabled = true;
			holdingWood = true;
			inventory.hasWood = true;
		}
	}

	private void PlaceBodyBridge(RaycastHit outpt)
	{
		//drop body if it is dragged too far away from the player
		Physics.IgnoreCollision(body.GetComponent<Collider>(), GetComponent<Collider>(), false);
		body.GetComponent<Rigidbody>().useGravity = false;
		body.GetComponent<Rigidbody>().isKinematic = true;
		body.GetComponent<BodyControl>().isHeld = false;
		body.transform.parent = null;
		body.transform.GetComponent<Rigidbody>().drag = 0f;
		body.transform.position = outpt.collider.gameObject.transform.position;
		body.transform.rotation = Quaternion.Euler(90, outpt.collider.gameObject.transform.eulerAngles.y, 0);
		isHolding = false;
	}

	private void DropBody()
	{
		GetComponent<PhotonView>().RPC("networkHandleBodyRelease", PhotonTargets.OthersBuffered, body.name);
		if (body) {
            body.transform.position = transform.position + Vector3.up;
            body.transform.rotation = Quaternion.Euler(90, transform.rotation.eulerAngles.y, 180);
			Physics.IgnoreCollision(body.GetComponent<Collider>(), GetComponent<Collider>(), false);
			body.GetComponent<Collider>().isTrigger = false;	
			body.GetComponent<Rigidbody>().useGravity = true;
			body.GetComponent<Rigidbody>().isKinematic = false;
            body.GetComponent<BodyControl>().isHeld = false;
            body.layer = 9;
		}
		body.transform.parent = null;
		body.transform.GetComponent<Rigidbody>().drag = 5f;
		
		body.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward);
		isHolding = false;
	}

	private void DropWood()
	{
        GetComponent<PhotonView>().RPC("networkHandleWoodRelease", PhotonTargets.OthersBuffered, body.name);
        if (body)
        {
            body.transform.position = transform.position + Vector3.up;
            body.transform.rotation = Quaternion.Euler(90, transform.rotation.eulerAngles.y, 0);
			Physics.IgnoreCollision(body.GetComponent<Collider>(), GetComponent<Collider>(), false);
			body.GetComponent<Collider>().isTrigger = false;	
			body.GetComponent<Rigidbody>().useGravity = true;
			body.GetComponent<Rigidbody>().isKinematic = false;
            body.GetComponent<FirewoodScript>().isHeld = false;
            body.layer = 9;
		}
		body.transform.parent = null;
		body.transform.GetComponent<Rigidbody>().drag = 5f;
		
		body.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward);
		holdingWood = false;
		inventory.hasWood = false;
	}

	private void ThrowBody()
	{
		if (isHolding)
		{
			GetComponent<PhotonView>().RPC("networkHandleBodyThrow", PhotonTargets.OthersBuffered, body.name);
			if (body){
				Physics.IgnoreCollision(body.GetComponent<Collider>(), GetComponent<Collider>(), false);
				body.GetComponent<Collider>().isTrigger = false;	
				body.GetComponent<Rigidbody>().useGravity = true;
				body.GetComponent<Rigidbody>().isKinematic = false;
				body.GetComponent<BodyControl>().isHeld = false;
			}
			body.transform.parent = null;
			body.transform.GetComponent<Rigidbody>().drag = 0f;
			
			body.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * throwForce);
			isHolding = false;
		}
	}
	#endregion

	public void respawn(){
		GameObject [] spawnPoints = GameObject.FindGameObjectsWithTag("HumanSpawn");
		if (spawnPoints == null) {
			Debug.Log ("No spawn points in level!");
			return;
		}
		GameObject mySpawn = spawnPoints [Random.Range (0, spawnPoints.Length)];
		transform.position = mySpawn.transform.position;
		transform.rotation = mySpawn.transform.rotation;

		hp = 100;
		isHolding = false;
		body = null;
	}

    public void OnDestroy()
    {
        if (PhotonNetwork.isMasterClient)
            DropItems();
    }

	#region Network RPC Calls
	//Network corpse physics object functionality
	[RPC]
	public void networkHandleBodyRelease(string bodyName){
		GameObject ntwkBody = GameObject.Find(bodyName);
		if (ntwkBody) {
			ntwkBody.GetComponent<Collider>().isTrigger = false;	
			ntwkBody.GetComponent<Rigidbody>().useGravity = true;
			ntwkBody.GetComponent<Rigidbody>().isKinematic = false;
			ntwkBody.GetComponent<BodyControl>().isHeld = false;
		}
	}

	[RPC]
	public void networkHandleWoodRelease(string bodyName){
		GameObject ntwkBody = GameObject.Find(bodyName);
		if (ntwkBody) {
			ntwkBody.GetComponent<Collider>().isTrigger = false;	
			ntwkBody.GetComponent<Rigidbody>().useGravity = true;
			ntwkBody.GetComponent<Rigidbody>().isKinematic = false;
			ntwkBody.GetComponent<FirewoodScript>().isHeld = false;
		}
	}

	[RPC]
	public void networkHandleBodyThrow(string bodyName){
		GameObject ntwkBody = GameObject.Find(bodyName);
		if (ntwkBody){
			ntwkBody.GetComponent<Collider>().isTrigger = false;	
			ntwkBody.GetComponent<Rigidbody>().useGravity = true;
			ntwkBody.GetComponent<Rigidbody>().isKinematic = false;
			ntwkBody.GetComponent<BodyControl>().isHeld = false;
		}
	}

	[RPC]
	public void networkHandleBodyPickup(string bodyName){
		GameObject ntwkBody = GameObject.Find(bodyName);
		if (ntwkBody){	
			ntwkBody.GetComponent<Collider>().isTrigger = true;			
			ntwkBody.GetComponent<Rigidbody>().useGravity = false;
			ntwkBody.GetComponent<Rigidbody>().isKinematic = true;
			ntwkBody.GetComponent<BodyControl>().isHeld = false;
		}
	}

	[RPC]
	public void networkHandleWoodPickup(string bodyName){
		GameObject ntwkBody = GameObject.Find(bodyName);
		if (ntwkBody){	
			ntwkBody.GetComponent<Collider>().isTrigger = true;	
			ntwkBody.GetComponent<Rigidbody>().useGravity = false;
			ntwkBody.GetComponent<Rigidbody>().isKinematic = true;
			ntwkBody.GetComponent<FirewoodScript>().isHeld = false;
		}
	}
	

	//Networking Flashlight mechanics
	public void receiveEMPBlast(){
		if (gameObject.tag == "Human") {
			GetComponent<PhotonView>().RPC("turnOffLight", PhotonTargets.AllBufferedViaServer, null);
			GetComponent<PhotonView>().RPC ("lanternCooldown", PhotonTargets.AllBufferedViaServer, null);
		}
	}

	[RPC]
	public void lanternCooldown()
	{
		StartLanternCooldown ();
	}

	[RPC]
	public void turnOffLight(){
		transform.Find("Flashlight").GetComponent<Light>().enabled = false;
	}

    [RPC]
    public void updateHealth()
    {
        gameObject.GetComponent<HumanControls>().hp = this.hp;
    }

    
    [RPC]
    public void updateComfort(System.Single newValue)
    {
    }

	[RPC]
	public void turnOnLight(){
		transform.Find("Flashlight").GetComponent<Light>().enabled = true;
		transform.Find("Flashlight").GetComponent<Light>().spotAngle = 0.0f;
	}

    [RPC]
    public void networkPushBack(Vector3 vectorToBePushedBack, float pushBackForce, float damage)
    {
        this.hp -= (int)damage;
        this.GetComponent<ImpactReceiver>().AddImpact(vectorToBePushedBack, pushBackForce);
    }

	[RPC]
	public void destroyHuman()
	{
		Destroy (gameObject);
	}

	[RPC]
	public void networkedPlaySound(string soundType)
	{
		if (soundType == "wood")
		{
			PlayWoodCall();
		}
		else if (soundType == "matches")
		{
			PlayMatchCall();
		}
		else if (soundType == "idle")
		{
			PlayIdleCall();
		}
		else if (soundType == "gas")
		{
			PlayGasCall();
		}
		else if (soundType == "body")
		{
			PlayBodyCall();
		}
	}

    [RPC]
    public void networkedPlaySoundWithCallback()
    {
        PlaySoundWithCallback(deathScreams[Random.Range(0, deathScreams.Length)], AudioFinished);
    }
	#endregion


}
