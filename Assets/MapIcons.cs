﻿using UnityEngine;
using System.Collections;

public class MapIcons : MonoBehaviour {

    GameObject fire1Icon;
    GameObject fire2Icon;
    GameObject fire3Icon;

	// Use this for initialization
	void Start () {

        fire1Icon = transform.Find("Fire1").gameObject;
        fire2Icon = transform.Find("Fire2").gameObject;
        fire3Icon = transform.Find("Fire3").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void EnableFire(int num)
    {
        switch(num)
        {
            case 1:
                {
                    //<PhotonView>().RPC("EnableFire1", PhotonTargets.AllBufferedViaServer);
                    break;
                }
            case 2:
                {
                    //GetComponent<PhotonView>().RPC("EnableFire2", PhotonTargets.AllBufferedViaServer);
                    break;
                }
            case 3:
                {
                    //GetComponent<PhotonView>().RPC("EnableFire3", PhotonTargets.AllBufferedViaServer);
                    break;
                }
        }
    }

    [RPC]
    public void EnableFire1()
    {
        fire1Icon.GetComponent<SpriteRenderer>().enabled = true;
    }

    [RPC]
    public void EnableFire2()
    {
        fire2Icon.GetComponent<SpriteRenderer>().enabled = true;
    }

    [RPC]
    public void EnableFire3()
    {
        fire3Icon.GetComponent<SpriteRenderer>().enabled = true;
    }
}
