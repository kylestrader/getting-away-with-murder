﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SoundBank))]
public class DoorHinge : Interactable {

    public int mClosedAngle;
    public int mOpenAngle;
    private float mActualClosedAngle;
    private float mActualOpenAngle;

    public bool mIsLocked;
    public GameObject mDoorHinge;

    private int mTargetAngle;
    public bool mIsOpen = false;

	// Use this for initialization
	void Start () {
        mActualClosedAngle = transform.eulerAngles.y + mClosedAngle;
        mTargetAngle = (int)mActualClosedAngle;
        mActualOpenAngle = transform.eulerAngles.y + mOpenAngle;
	}
	
	// Update is called once per frame
	void Update () {
        if (mDoorHinge.transform.eulerAngles.y != mTargetAngle)
        {
            mDoorHinge.transform.rotation = Quaternion.Lerp(mDoorHinge.transform.rotation, Quaternion.Euler(mDoorHinge.transform.eulerAngles.x, mTargetAngle, mDoorHinge.transform.eulerAngles.z), Time.deltaTime * 12);
        }
	}

    public override void Interact(bool isGhost)
    {
        if (isGhost)
        {
            this.GetComponent<PhotonView>().RPC("ghostInteractDoor", PhotonTargets.AllBufferedViaServer);
        }
        else
        {
            if (!mIsLocked)
            {
                if (mTargetAngle == (int)mActualClosedAngle)
                {
                    this.GetComponent<PhotonView>().RPC("openDoor", PhotonTargets.AllBufferedViaServer);
                }
                else if (mTargetAngle == (int)mActualOpenAngle)
                {
                    this.GetComponent<PhotonView>().RPC("closeDoor", PhotonTargets.AllBufferedViaServer);
                }
            }

            else
            {
                this.GetComponent<PhotonView>().RPC("openLockedDoor", PhotonTargets.AllBufferedViaServer);
            }

        }


    }

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
	}

    [RPC]
    public void openDoor()
    {
        GetComponent<SoundBank>().Play("Door-Handle");
        mTargetAngle = (int)mActualOpenAngle;
        mIsOpen = true;
    }

    [RPC]
    public void closeDoor()
    {
        GetComponent<SoundBank>().Play("Door-Close");
        mTargetAngle = (int)mActualClosedAngle;
        mIsOpen = false;
    }

    [RPC]
    public void closeAndLockDoor()
    {
        GetComponent<SoundBank>().QueuePlay("Door-Close");
        GetComponent<SoundBank>().QueuePlay("Door-Lock");
        mTargetAngle = mClosedAngle;
        mIsOpen = false;
        this.mIsLocked = true;
    }

    [RPC]
    public void openLockedDoor()
    {
        GetComponent<SoundBank>().Play("Door-Locked");
    }

    [RPC]
    public void lockDoor()
    {
        GetComponent<SoundBank>().Play("Door-Lock");
        this.mIsLocked = true;
    }

    [RPC]
    public void ghostInteractDoor()
    {
        //close the door if it is open
        if(GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().ghost.GetComponent<GhostControls>().ghostPower >= 4)
            if (this.mIsOpen)
                this.GetComponent<PhotonView>().RPC("closeAndLockDoor", PhotonTargets.AllBufferedViaServer);
            else
                this.GetComponent<PhotonView>().RPC("lockDoor", PhotonTargets.AllBufferedViaServer);
    }
}
