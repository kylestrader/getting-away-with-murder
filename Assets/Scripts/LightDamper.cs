﻿using UnityEngine;
using System.Collections;

public class LightDamper : MonoBehaviour {

	private float distToGhost;
	private float maxRange;

	// Use this for initialization
	void Start () {
		maxRange = GetComponent<Light> ().range;
	}
	
	// Update is called once per frame
	void Update () {

		CheckDistanceToGhost ();
		ChangeLightLevels ();
	}

	void CheckDistanceToGhost()
	{
		if (GameObject.Find("GhostController(Clone)") != null)
		{
			Vector3 ghostPos = GameObject.Find ("GhostController(Clone)").transform.position;
			Vector3 ghostToLight = ghostPos - transform.position;
			distToGhost = ghostToLight.magnitude;
			//Debug.Log(distToGhost);
		}
		else distToGhost = 100;
	}

	void ChangeLightLevels()
	{
		float intensity = distToGhost / 2;
		if (intensity > maxRange) intensity = maxRange;
		GetComponent<Light> ().range = intensity;
	}
}
