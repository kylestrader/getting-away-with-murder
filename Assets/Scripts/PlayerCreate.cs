﻿using UnityEngine;
using System.Collections;

public class PlayerCreate : Photon.MonoBehaviour {

    public GameObject gameEnvMgr;
    public GameObject personalData;
	public Camera standByCamera;

    private GameObject[] spawnPoints;

	public GameObject HumanUI;
	public GameObject GhostUI;

	public int numMurderers = 0;
	public int numGhosts = 0;

	void Start(){
		if (System.Object.ReferenceEquals(null, HumanUI))
		{
			Debug.LogError("HumanUI prefab could not be found. Try setting the HumanUI in the inspector.");
		}

		if (System.Object.ReferenceEquals(null, GhostUI))
		{
			Debug.LogError("GhostUI prefab could not be found. Try setting the GhostUI in the inspector.");
		}
	}
	
	//Network Interface
	[RPC]
	void playerEnteredRoom(bool isGhost)
	{
		if (isGhost) numGhosts++;
		else numMurderers++;
	}
	
	[RPC]
	void playerLeftRoom(bool isGhost)
	{
		if (isGhost) numGhosts--;
		else numMurderers--;
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){}

	public void SpawnMyPlayer(bool isGhost) {
		//Get player ready for game session
		if (isGhost && numGhosts == 0)
		{
			CreateGhostPlayer();
		}
		else if (!isGhost /*&& numMurderers == 0*/)
		{
			CreateHumanPlayer();
		}
	}

	public void CreateHumanPlayer()
	{
		//human is always red
		PhotonNetwork.player.SetTeam(PunTeams.Team.red);

		//Alert other players that a player has entered the room and is now playing in the current session (not spectating)
		photonView.RPC("playerEnteredRoom", PhotonTargets.AllBuffered, false/*player is not a ghost*/);

		//find human spawns
		spawnPoints = GameObject.FindGameObjectsWithTag("HumanSpawn");

		//no human spawns
		if (System.Object.ReferenceEquals(null, spawnPoints))
		{
			Debug.LogError("No spawn points for HUMAN in level!");
			return;
		}

		//find random spawn point
		GameObject mySpawn = spawnPoints[Random.Range(0, spawnPoints.Length)];

		//create human
		GameObject myPlayer = null;
		myPlayer = PhotonNetwork.Instantiate("HumanController", mySpawn.transform.position, mySpawn.transform.rotation, 0);
        GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().human = myPlayer;

		//Enable player movement
		((MonoBehaviour)myPlayer.GetComponent("FPSInputController")).enabled = true;
		((MonoBehaviour)myPlayer.GetComponent("CharacterMotor")).enabled = true;
		((MonoBehaviour)myPlayer.GetComponent("HumanControls")).enabled = true;

		//turn flashlight on
		myPlayer.transform.Find("Flashlight").GetComponent<FlashlightController>().enabled = true;

        //Turn map on
        myPlayer.transform.Find("MapPlane").gameObject.SetActive(true);

		//turn player camera on
        myPlayer.transform.Find("Main Camera").gameObject.SetActive(true);
        GameObject cam = myPlayer.transform.Find("Map Camera").gameObject;
        cam.SetActive(true);
        myPlayer.transform.Find("Doctor").GetComponent<MeshRenderer>().enabled = false;
        myPlayer.transform.Find("Outline").GetComponent<MeshRenderer>().enabled = false;
        myPlayer.transform.Find("MapPlane").GetComponent<MeshRenderer>().enabled = true;

		GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("PlayerSelect").SetActive(false);
		//create the humans UI

		GameObject.Find("MainMenuCamera").GetComponent<AudioListener>().enabled = false;
        GameObject.Find("MainMenuCamera").GetComponent<Light>().enabled = false;
		GameObject.Find ("_SCRIPTS").GetComponent<MenuHandler> ().gameMenus.Add (GameObject.Instantiate(HumanUI));

        gameEnvMgr.GetComponent<GameEnvironment>().addHuman(myPlayer.name);
        personalData.GetComponent<MyData>().Instantiate(false, myPlayer);

		GameObject.Find ("_GLOBAL_AMBIENT_SPEAKER").GetComponent<SoundBank> ().Play ("ingamemusic_edit");
	}

	public void CreateGhostPlayer()
	{
		//ghost is always blue
		PhotonNetwork.player.SetTeam(PunTeams.Team.blue);

		//Alert other players that a player has entered the room and is now playing in the current session (not spectating)
		photonView.RPC("playerEnteredRoom", PhotonTargets.AllBuffered, true/*player is a ghost*/);

		//find human spawns
		spawnPoints = GameObject.FindGameObjectsWithTag("GhostSpawn");

		//no human spawns
		if (System.Object.ReferenceEquals(null, spawnPoints))
		{
			Debug.LogError("No spawn points for GHOST in level!");
			return;
		}

		//find random spawn point
		GameObject mySpawn = spawnPoints[Random.Range(0, spawnPoints.Length)];

		//create human
		GameObject myPlayer = null;
		myPlayer = PhotonNetwork.Instantiate("GhostController", mySpawn.transform.position, mySpawn.transform.rotation, 0);
        GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().ghost = myPlayer;

		//Disable the stand-by camera
		//standByCamera.gameObject.SetActive(false);

		//Enable player movement
		((MonoBehaviour)myPlayer.GetComponent("FPSInputController")).enabled = true;
		((MonoBehaviour)myPlayer.GetComponent("CharacterMotor")).enabled = true;
		((MonoBehaviour)myPlayer.GetComponent("GhostControls")).enabled = true;

		//turn flashlight on
		myPlayer.transform.Find("Point light").GetComponent<Light>().enabled = true;

		//turn player camera on
		myPlayer.transform.Find("Main Camera").gameObject.SetActive(true);

		GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("PlayerSelect").SetActive(false);
		//create the Ghost UI

		GameObject.Find ("_SCRIPTS").GetComponent<MenuHandler> ().gameMenus.Add (GameObject.Instantiate(GhostUI));
		GameObject.Find("MainMenuCamera").GetComponent<AudioListener>().enabled = false;
        GameObject.Find("MainMenuCamera").GetComponent<Light>().enabled = false;

        gameEnvMgr.GetComponent<GameEnvironment>().addGhost(myPlayer.name);
        personalData.GetComponent<MyData>().Instantiate(true, myPlayer);

		GameObject.Find ("_GLOBAL_AMBIENT_SPEAKER").GetComponent<SoundBank> ().Play ("ingamemusic_edit");
	}
}
