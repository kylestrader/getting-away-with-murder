﻿using UnityEngine;
using System.Collections;

public class FollowSight : MonoBehaviour {
	
	
	GameObject cam = null;

	void Start(){
		cam = transform.parent.gameObject.transform.Find ("Main Camera").gameObject;
	}
	
	// Update is called once per frame
	void Update()
	{
			Quaternion rot = cam.transform.rotation;
			transform.rotation = rot;
	}
}
