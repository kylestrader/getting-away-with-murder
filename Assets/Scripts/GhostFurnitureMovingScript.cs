﻿using UnityEngine;
using System.Collections;

public class GhostFurnitureMovingScript : MonoBehaviour {

    private Camera ghostCamera;


    //Moving Furnature Variables
    private bool isMovingFurnature;
    private RaycastHit moveFurnatureRay;
    private CharacterMotor characterMotor;
    public float pushFactor = 1.0f;
    public float maxFurnaturePushRange = 2.0f;
    public float maxFurnitureRange = 1.0f;
    public float rangeToPushFurniture = 2.0f;


    public float maxDraggingForwardSpeed = 0.0f;
    public float maxDraggingSidewaysSpeed = 0.0f;
    public float maxDraggingBackwardsSpeed = 0.0f;

    private float maxForwardSpeed = 0.0f;
    private float maxSidewaysSpeed = 0.0f;
    private float maxBackwardsSpeed = 0.0f;

	// Use this for initialization
	void Start () {

        characterMotor = transform.GetComponent<CharacterMotor>();

        ghostCamera = transform.Find("Main Camera").GetComponent<Camera>();
        maxForwardSpeed = characterMotor.movement.maxForwardSpeed;
        maxSidewaysSpeed = characterMotor.movement.maxSidewaysSpeed;
        maxBackwardsSpeed = characterMotor.movement.maxBackwardsSpeed;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        pushFurniture();
	
	}


    private void pushFurniture()
    {


        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Ghost Clisk");
            Debug.DrawRay(ghostCamera.transform.position, ghostCamera.transform.forward, Color.white, 2);
            //See if we hit a furniture object

            if (Physics.Raycast(ghostCamera.transform.position + Vector3.down, ghostCamera.transform.forward * rangeToPushFurniture, out moveFurnatureRay))
            {
                if (moveFurnatureRay.collider.gameObject.tag == "Furniture")
                {
                    characterMotor.movement.maxForwardSpeed = maxDraggingForwardSpeed;
                    characterMotor.movement.maxBackwardsSpeed = maxDraggingBackwardsSpeed;
                    characterMotor.movement.maxSidewaysSpeed = maxDraggingSidewaysSpeed;
                    isMovingFurnature = true;

                }
            }
        }

        if (isMovingFurnature)
        {

            //check if still in range
            if (Vector3.Distance(ghostCamera.transform.position, moveFurnatureRay.collider.gameObject.transform.position) > maxFurnaturePushRange)
            {
                isMovingFurnature = false;
                characterMotor.movement.maxForwardSpeed = maxForwardSpeed;
                characterMotor.movement.maxBackwardsSpeed = maxBackwardsSpeed;
                characterMotor.movement.maxSidewaysSpeed = maxSidewaysSpeed;
            }
            else
            {
                moveFurnatureRay.collider.gameObject.GetComponent<Rigidbody>().AddForce(Input.GetAxis("Vertical") * this.transform.forward * pushFactor + Input.GetAxis("Horizontal") * this.transform.right * pushFactor, ForceMode.Acceleration);
                if (moveFurnatureRay.collider.gameObject.GetComponent<Rigidbody>().velocity.magnitude > maxFurnitureRange)
                {
                    moveFurnatureRay.collider.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;//; moveFurnatureRay.collider.gameObject.rigidbody.velocity.normalized * 1;
                }

            }


        }

        if (Input.GetMouseButtonUp(1))
        {
            isMovingFurnature = false;
            characterMotor.movement.maxForwardSpeed = maxForwardSpeed;
            characterMotor.movement.maxBackwardsSpeed = maxBackwardsSpeed;
            characterMotor.movement.maxSidewaysSpeed = maxSidewaysSpeed;
        }
    }
}
