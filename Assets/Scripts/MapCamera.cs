﻿using UnityEngine;
using System.Collections;

public class MapCamera : MonoBehaviour {

	Texture2D renderTex;
	public Material renderMat;

	float timeBetweenUpdates = 2.0f;
	float timeSinceLastUpdate = 0.0f;

	// Use this for initialization
	void Start () {

        this.GetComponent<AudioListener>().enabled = false;
		renderTex = new Texture2D (Screen.width, Screen.height);
		renderMat.mainTexture = renderTex;
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnPostRender()
	{
		if (timeSinceLastUpdate > timeBetweenUpdates)
		{
			renderTex.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
			renderTex.Apply ();

			timeSinceLastUpdate = 0;
		}
		else timeSinceLastUpdate += Time.deltaTime;
	}

}
