﻿using UnityEngine;
using System.Collections;

public class SpikeTrap : MonoBehaviour {

	public bool isTriggered = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Human" || other.gameObject.tag == "Body")
		{
			if (other.gameObject.tag == "Human")
			{
                other.gameObject.GetComponent<PhotonView>().RPC("updateHealth", PhotonTargets.All, null);
			}
		}
		GetComponent<PhotonView> ().RPC ("activate", PhotonTargets.All, other.gameObject.tag);
	}

	[RPC]
	void activate(string tag)
	{
		if (tag == "Human" || tag == "Body")
		{
			isTriggered = true;
		}
	}
}
