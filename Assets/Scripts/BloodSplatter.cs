﻿using UnityEngine;
using System.Collections;

public class BloodSplatter : MonoBehaviour
{

    public float fadeTimer;
    private float timer = 0;





	// Use this for initialization
	void Start () {

        

        if (GameObject.Find("PlayerControllerHuman(Clone)").GetComponent<PhotonView>().isMine)
        {
            Behaviour halo = (Behaviour)GetComponent("Halo");
            halo.enabled = false;
        }
        else
        {
            Behaviour halo = (Behaviour)GetComponent("Halo");
            halo.enabled = true;

        }
        this.transform.parent = GameObject.Find("BloodSplats").transform;

	}
	
	// Update is called once per frame
	void Update () {



        timer += Time.deltaTime;
        Color tempColor = this.gameObject.GetComponent<Renderer>().material.color;
        tempColor.a =  1-(timer/fadeTimer);
        this.gameObject.GetComponent<Renderer>().material.color = tempColor;

        if (timer >= fadeTimer)
        {
            Destroy(this.gameObject);
            timer = 0;
        }
            

	
	}
}
