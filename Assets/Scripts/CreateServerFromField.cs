﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateServerFromField : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateRoomFromField()
	{
		string name = GetComponent<InputField> ().text;
		if (name == null) name = "default";
		
		GameObject.Find ("_SCRIPTS").GetComponent<NetworkManager> ().CreateRoom (name, true, true, false, "", 5);
	}

	public void JoinRoomFromField()
	{
		string name = GetComponent<InputField> ().text;
		if (name == null) name = "default";

		GameObject.Find ("_SCRIPTS").GetComponent<NetworkManager> ().JoinRoom (name);
	}
}
