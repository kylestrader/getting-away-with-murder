﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        this.GetComponent<Image>().color = Color.Lerp(this.GetComponent<Image>().color,Color.black,Time.deltaTime * 0.5f);
	
	}
}
