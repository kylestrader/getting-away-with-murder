﻿using UnityEngine;
using System.Collections;

public class Footsteps : MonoBehaviour 
{
	public AudioClip [] stepsWood;
    public AudioClip[] landWood;
	
	public float walkAudioSpeed = 0.4f;
	private AudioClip walk;	
	private float walkAudioTimer = 0.0f;	
	bool isWalking = false;
	//private float walkSpeed = 0.0f;	
	private CharacterController chCtrl;

    bool prevGrounded = false;
	
	void Start()
	{
		chCtrl = GetComponent<CharacterController>();
	}
	
	void Update()
	{
		if ( chCtrl.isGrounded )
		{
            if (!prevGrounded)
            {
                PlayLanding();
            }

            else PlayFootsteps();
		}
		else
		{
			walkAudioTimer = 0.0f;
		}

        prevGrounded = chCtrl.isGrounded;
	}
	
	void PlayFootsteps() 
	{
		if ( Input.GetAxis( "Horizontal" ) != 0.0f || Input.GetAxis( "Vertical" ) != 0.0f )
		{
			isWalking = true;
			walkAudioTimer += Time.deltaTime;
		}
		else
		{
			isWalking = false;
			walkAudioTimer = 0.0f;
		}
		
		// Play Audio
		if ( isWalking )
		{
			if ( walkAudioTimer > walkAudioSpeed  && !GetComponent<AudioSource>().isPlaying)
			{
				GetComponent<AudioSource>().clip = stepsWood[Random.Range(0, stepsWood.Length)];
				GetComponent<AudioSource>().volume = 0.25f;
				GetComponent<AudioSource>().Play();
				walkAudioTimer = 0.0f;
			}
		}  
	}

    void PlayLanding()
    {
		GetComponent<AudioSource>().volume = 0.25f;
        GetComponent<AudioSource>().clip = landWood[Random.Range(0, landWood.Length)];
        GetComponent<AudioSource>().Play();
        walkAudioTimer = 0.0f;
    }

}
