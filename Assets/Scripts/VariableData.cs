﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("VariableData")]
public class VariableData
{
	#region Traps
	public int MirrorDmg;
	#endregion
	
	#region Human
	public float MaxHealth;
	public float HealthRegen;
	public float BodyThrowForce;
	public float LanternActivationSpd;
	public float LanternReactivateCD;
	public int FoodHeal;
	public int MaxNumVials;
	#endregion
	
	#region Ghost
	public float SpookySndCD;
	public float ColdAuraCD;
	public float VisibilityCD;
	public float SpookySndDmg;
	public float VisibilityDmg;
	public float RespawnTime;
	public float VisibilityTime;
	public bool CanInvisibleAtWill;
	public float MemoryPowerAmt;
	public float LightDisableRadius;
	#endregion
	
	#region Body
	public float BloodDripSpeed;
	#endregion
	
	#region SafeZone
	public float ComfortHeal;
	public float HealBeforeDrained;
	#endregion
}