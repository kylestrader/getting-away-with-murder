﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class SoundBank : MonoBehaviour 
{
	public AudioClip [] soundBank;
    private Queue<AudioClip> queuedList;
    AudioClip soundToPlay;

    public void Start()
    {
        queuedList = new Queue<AudioClip>();
    }

    public void Update()
    {
        try
        {
            if (!GetComponent<AudioSource>().isPlaying && queuedList.Count > 0)
            {
                AudioClip tmp = queuedList.Dequeue();
                Play(tmp);
            }
        }
        catch
        {
            Debug.LogWarning("SOUND BANK WARNING");
        }

    }

	public bool Play(string soundName)
    {
        bool valid = false;
		foreach (AudioClip ac in soundBank) 
        {
			if (ac.name == soundName)
            {
				soundToPlay = ac;
                valid = true;
				break;
			}
		}
		if (valid)
        {
            GetComponent<AudioSource>().clip = soundToPlay;
            GetComponent<AudioSource>().Play();
            return true;
		}
        else
        {
            return false;
        }
	}

    public void Play(AudioClip audioClip)
    {
        soundToPlay = audioClip;
        if (!System.Object.ReferenceEquals(soundToPlay, null))
        {
            GetComponent<AudioSource>().clip = soundToPlay;
            GetComponent<AudioSource>().Play();
        }
    }

    public bool QueuePlay(string soundName)
    {
        foreach (AudioClip ac in soundBank)
        {
            if (ac.name == soundName)
            {
                queuedList.Enqueue(ac);
                return true;
            }
        }
        return false;
    }

    public void Stop()
    {
        GetComponent<AudioSource>().Stop();
    }
}
