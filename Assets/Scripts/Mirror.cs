﻿using UnityEngine;
using System.Collections;

public class Mirror : Interactable {

	// Use this for initialization
    private RenderTexture rt;
    private Camera theCamera;
	private bool isDestroyed;

	void Start () {
        rt = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
        rt.Create();
         theCamera = GetComponentInChildren<Camera>();
        theCamera.GetComponent<AudioListener>().enabled = false;
        theCamera.targetTexture = rt;
        this.gameObject.GetComponent<Renderer>().material.mainTexture = rt;
        this.gameObject.GetComponent<Renderer>().material.SetTexture("Metallic",rt);
		isDestroyed = false;
	}
	
	// Update is called once per frame
	void Update () {
        //Vector3 cameraPoint = Camera.main.WorldToViewportPoint(this.transform.position);
	    //if(cameraPoint.x > 0 &&cameraPoint.y > 0 && cameraPoint.x < 1 && cameraPoint.y < 1 && cameraPoint.z >0)
        //{
        //    //theCamera.enabled = true;
        //}
        //else
        //{
        //    theCamera.enabled = false;
        //}
	}

    public void OnWillRenderObject()
    {
        //theCamera.enabled = true;
    }

	void OnTriggerEnter(Collider other)
	{

	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
	}

	public override void Interact(bool isGhost)
	{
		if (isGhost && !isDestroyed)
		{
            if (GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().ghost.GetComponent<GhostControls>().ghostPower >= 2)
			gameObject.GetComponentInChildren<ParticleSystem>().Play();
			isDestroyed = true;
			GetComponent<PhotonView>().RPC("networkBreakMirror", PhotonTargets.All);
			Debug.Log("Ghost interacted - destroy mirror");
		}
	}

	[RPC]
	public void networkBreakMirror(){
		gameObject.GetComponentInChildren<ParticleSystem> ().Play ();
		gameObject.GetComponentInChildren<MirrorShards> ().exists = true;
	}
}
