﻿using UnityEngine;
using System.Collections;

/* File: Network Interpolation
 * Purpose: When attached to a Game Object, this script will act as an interface between players in the room. 
 *  This interface interpolates the position and rotation of an object.
 * Bugs: N/A
 */

[RequireComponent(typeof(Transform))]
[RequireComponent(typeof(PhotonView))]
public class NetworkInterpolation : Photon.MonoBehaviour
{
    //PUBLIC MEMBERS
    //->Synchronization toggles
    public float PredictionAmmount = 0f;
    public bool synchronizeRotation = true;
    public bool syncronizePosition = true;

    //PRIVATE MEMBERS
    //->This is what gets passed through the photon serializer and ghets sent to its observers
    //-->Target Rotation
    private Quaternion realRotation = Quaternion.identity;
    private Quaternion realRotationVelocity = Quaternion.identity; //Used to help predict the rotation
    //-->Target Position
    private Vector3 realPosition = Vector3.zero;
    private Vector3 realPositionVelocity; //Used to help predict the position

    //local predictive information
    //->Rotation sync
    private  Quaternion lastRotation = Quaternion.identity;
    private Quaternion currentRotation = Quaternion.identity;
    //->Position sync
    private Vector3 lastPosition = Vector3.zero;
    private Vector3 currentPosition = Vector3.zero;

    //Observers predictive logic
    private float syncTime = 0f;
    private float syncDelay = 0f;
    private float lastSynchronizationTime = 0f;
    //->Rotation sync
    private Quaternion syncStartRotation = Quaternion.identity;
    private Quaternion syncEndRotation = Quaternion.identity;
    //->Position Sync
    private Vector3 syncStartPosition = Vector3.zero;
    private Vector3 syncEndPosition = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        syncEndPosition = this.transform.position;
        syncStartPosition = this.transform.position;
        syncEndRotation = this.transform.rotation;
        syncStartRotation = this.transform.rotation;


        currentPosition = this.transform.position;
        currentRotation = this.transform.rotation;

        realPosition = this.transform.position;
        realRotation = this.transform.rotation;
        lastRotation = this.transform.rotation;
        lastPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //If this object is local to the player (owned by the player)
        if (photonView.isMine)
        {
            realPosition = transform.position;
            syncStartPosition = transform.position;
            syncEndPosition = transform.position;

            realRotation = transform.rotation;
            syncStartRotation = transform.rotation;
            syncEndRotation = transform.rotation;

            //Setup next update call
            lastRotation = currentRotation;
            lastPosition = currentPosition;

            //Prep Rotation and angular velocity (rotational velocity) for serialization
            currentRotation = transform.rotation;
            Quaternion rotDiff = Quaternion.Euler(currentRotation.eulerAngles - lastRotation.eulerAngles);
            realRotationVelocity = Quaternion.Euler(rotDiff.eulerAngles / Time.deltaTime);

            //Prep Position and positional velocity for serialization
            currentPosition = transform.position;
            Vector3 posDiff = currentPosition - lastPosition;
            realPositionVelocity = posDiff / Time.deltaTime;
        }

        //If the object is being observed by another player
        else
        {
            //Setup next update call
            lastRotation = currentRotation;
            lastPosition = currentPosition;

            currentRotation = transform.rotation;
            currentPosition = transform.position;
            syncTime += Time.deltaTime * 9;
            transform.rotation = Quaternion.Lerp(syncStartRotation, syncEndRotation, syncTime);
            transform.position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime);
        }
    }

    //Called every time serialized information is sent or retrieved from this object
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //This is OUR player. We need to send our information to the network.
        if (stream.isWriting)
        {
            //Serialize rotation and angular velocity (rotational velocity)
            stream.SendNext(transform.rotation);
            stream.SendNext(realRotationVelocity);

            //Serialize position and positional velocity
            stream.SendNext(transform.position);
            stream.SendNext(realPositionVelocity);
        }

        //This is someone else's player. We need to receive their information.
        else
        {
            //Calculate time between packets
            syncTime = 0f;
            syncDelay = Time.time - lastSynchronizationTime;
            lastSynchronizationTime = Time.time;

            //Interpret rotational information
            realRotation = (Quaternion)stream.ReceiveNext();
            realRotationVelocity = (Quaternion)stream.ReceiveNext();

            Vector3 rotVelEulers = realRotationVelocity.eulerAngles;
            Quaternion rotationVelocityScaled = Quaternion.Euler(rotVelEulers.x * syncDelay,
                                                                 rotVelEulers.y * syncDelay,
                                                                 rotVelEulers.z * syncDelay); //Scale velocity to compensate for sync delay

            //Setup rotational information that needs to be interpolated
            syncEndRotation = Quaternion.Euler(realRotation.eulerAngles + (rotationVelocityScaled.eulerAngles * PredictionAmmount));
            syncStartRotation = transform.rotation;

            //Interpret positional information
            realPosition = (Vector3)stream.ReceiveNext();
            realPositionVelocity = (Vector3)stream.ReceiveNext();

            Vector3 positionVelocityScaled = realPositionVelocity * syncDelay; //Scale velocity to compensate for sync delay

            //Setup rotational information that needs to be interpolated
            syncEndPosition = realPosition + positionVelocityScaled * PredictionAmmount;
            syncStartPosition = transform.position;
        }
    }
}