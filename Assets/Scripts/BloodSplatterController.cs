﻿using UnityEngine;
using System.Collections;

public class BloodSplatterController : MonoBehaviour 
{

    private BodyControl theBody;
    private bool isHeld;
    public bool needsToBoHeld = false;

    private float timer;
    //In Seconds
    public float timeBetweenSplats;
    public GameObject bloodSplatPrefab;

	// Use this for initialization
	void Start () {
        theBody = GetComponent<BodyControl>();
        isHeld = theBody.isHeld;

	
	}
	
	// Update is called once per frame
	void Update () {
        isHeld = theBody.isHeld;
        timer += Time.deltaTime;
        if(timer >= timeBetweenSplats)
        {
            timer = 0;
            if (isHeld && needsToBoHeld)
            {
                dropBloodSplat();
            }
            else
                dropBloodSplat();

        }

	}

    private void dropBloodSplat()
    {
        RaycastHit rayHit;
        Ray ray = new Ray(transform.position,Vector3.down);
        if(Physics.Raycast(ray,out rayHit))
        {
            GameObject bloodSpat =(GameObject) Instantiate(bloodSplatPrefab, rayHit.point + new Vector3(Random.Range(-0.2f,0.2f),.01f,Random.Range(-0.2f,0.2f)), transform.rotation);
            bloodSpat.transform.localScale = new Vector3(Random.Range(0.5f, 1.5f), Random.Range(0.5f, 1.5f), 1);
            bloodSpat.transform.rotation = Quaternion.Euler(new Vector3(90, Random.Range(0,360), 0));
        }
    }
}
