﻿using UnityEngine;
using System.Collections;

public class SpectatorScript : MonoBehaviour {
    public GameEnvironment gameEnv;

    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes;
    float lookX_Amt = 0.0f;
    float lookY_Amt = 0.0f;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;


    GameObject optionsMenu;
    GameObject pauseMenu;
	// Use this for initialization
	void Start () {
        gameEnv = GameObject.Find("_GAME_ENV_MANAGER").GetComponent<GameEnvironment>();
        optionsMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("OptionsMenu");
        pauseMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("PauseMenu");
        axes = RotationAxes.MouseXAndY;
	}
	
	// Update is called once per frame
	void Update () {
        if(gameEnv == null)
        {
            gameEnv = GameObject.Find("_GAME_ENV_MANAGER").GetComponent<GameEnvironment>();
        }
        if(optionsMenu == null)
        {
            optionsMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("OptionsMenu");
        }
        if(pauseMenu == null)
        {
            pauseMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("PauseMenu");
        }

        HandleMouse();

        //Keyboard
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pauseMenu.GetActive() && !optionsMenu.GetActive())
            {
                pauseMenu.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else if (pauseMenu.GetActive())
            {
                pauseMenu.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else if (optionsMenu.GetActive())
            {
                optionsMenu.SetActive(false);
                pauseMenu.SetActive(true);
            }
        }
	
	}

    private void HandleMouse()
    {
        //Mouse
        float dx = Input.GetAxis("Mouse X");
        dx = dx * Mathf.Abs(dx);
        dx = Mathf.Clamp(dx, -.5f, .5f);
        float dy = Input.GetAxis("Mouse Y");
        dy = dy * Mathf.Abs(dy);
        dy = Mathf.Clamp(dy, -.5f, .5f);

        lookX_Amt = dx / 2; // Added to corpse target position relative to player
        lookY_Amt = dy / 2;

        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + dx * sensitivityX;
            rotationY += dy * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
            GetComponentInChildren<Camera>().transform.localEulerAngles = new Vector3(-rotationY, 0, 0);
            transform.localEulerAngles = new Vector3(0, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, dx * sensitivityX, 0);
        }
        else
        {
            rotationY += dy * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
            GetComponentInChildren<Camera>().transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }
    }
}
