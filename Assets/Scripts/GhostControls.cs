﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[AddComponentMenu("Camera-Control/Mouse Look")]
public class GhostControls: MonoBehaviour {

	public Material[] materials;
	
	private Camera ghostCamera;
    
    //great name isn't it, holds a refernce to the global variable component in _SCRIPTS
    private Variables variables;

    public bool isStunned = false;
	public bool isHolding = false;
	public bool isInvisible = true;
	public float lightRadius = 10.0f;

	public float ghostPower = 100.0f;

    //Player mouse information
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;

    //Movement Variables
    private float maxForwardSpeed = 0.0f;
    private float maxSidewaysSpeed = 0.0f;
    private float maxBackwardsSpeed = 0.0f;
    
    private float minForwardSpeed = 0.0f;
    private float minSidewaysSpeed = 0.0f;
    private float minBackwardsSpeed = 0.0f;

    
	// Cooldown stuff
	public float maxCooldown = 30.0f;
	public float currentCooldown = 0.0f;
	public float cooldownTimer = 0.0f;
	public float invisibilityTimerMax;
	public float invisibilityTimer = 0.0f;
	public float soundTimerMax;
	public float soundTimer = 0.0f;
	public bool soundPlayed = false;
	public float lanternCooldown;
	public float lanternTimerMax;
	public float lanternTimer = 0.0f;
	public bool lanternOff = false;


    public float wailCoolDown =0.0f;
    public bool canWail;
    public float wailTimerMax = 10.0f;
    public bool WailOnCoolDown = true;

	private float soundDmg;

	public bool canVisible = true;
	public Slider visCdSlider;
	public Slider lanternCdSlider;
    public Slider humanHealthSlider;
    public Slider humanComfortSlider;
    public Slider WailCooldDownSlider;

	public AudioClip[] deathScreams;

    private CharacterMotor characterMotor;

	public GameObject ghostGraphics;
	
    public float interactionRange = 100.0f;
    public float StunTime = 5.0f;
    private float ColorLerpTimer;

	// Use this for initialization
	void Start () {
		ghostCamera = transform.Find("Main Camera").GetComponent<Camera>();
        variables = GameObject.Find("_SCRIPTS").GetComponent<Variables>();
        if(!GetComponent<PhotonView>().isMine)
        {
            this.GetComponent<Camera>().GetComponent<AudioListener>().enabled = false;
            this.GetComponent<Camera>().enabled = false;
        }

        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;
	
        Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
        SetupCharacterMotor();
		SetupCooldowns ();
		GetVariableDefaults ();
	}

    private void SetupCharacterMotor()
    {
        characterMotor = GetComponent<CharacterMotor>();
        //stats initialization
        maxForwardSpeed =  characterMotor.movement.maxForwardSpeed;
        maxSidewaysSpeed = characterMotor.movement.maxSidewaysSpeed;
        maxBackwardsSpeed =characterMotor.movement.maxBackwardsSpeed;

        minForwardSpeed = characterMotor.movement.maxForwardSpeed * 0.7f;
        minSidewaysSpeed = characterMotor.movement.maxSidewaysSpeed * 0.7f;
        minBackwardsSpeed = characterMotor.movement.maxBackwardsSpeed * 0.7f;
    }

	private void GetVariableDefaults()
	{
		soundDmg = variables.SpookySndDmg;
		lightRadius = variables.LightDisableRadius;
	}

	void SetupCooldowns()
	{
		invisibilityTimerMax = variables.VisibilityCD;
		soundTimerMax =        variables.SpookySndCD;
		lanternTimerMax =      variables.ColdAuraCD;

		visCdSlider = GameObject.Find ("GhostUI(Clone)/VisSlider").GetComponent<Slider>();
		lanternCdSlider = GameObject.Find ("GhostUI(Clone)/FlashlightSlider").GetComponent<Slider>();

       // humanHealthSlider = GameObject.Find("GhostUI(Clone)/HumanHealth").GetComponent<Slider>();
       // humanComfortSlider = GameObject.Find("GhostUI(Clone)/HumanComfort").GetComponent<Slider>();

        WailCooldDownSlider = GameObject.Find("GhostUI(Clone)/WailSlider").GetComponent<Slider>();

        WailCooldDownSlider.GetComponent<Slider>().maxValue = wailTimerMax;
		visCdSlider.GetComponent<Slider> ().maxValue = invisibilityTimerMax;
		lanternCdSlider.GetComponent<Slider> ().maxValue = lanternTimerMax;
	}
	
	// Update is called once per frame
	void Update () {
	
		HandleInput ();
		ChangeUI ();
		computeCooldown ();
		drawCooldown ();


        GameObject.FindGameObjectWithTag("GhostUI").GetComponentInChildren<Image>().enabled = isInvisible;
        if(isStunned)
        {
            GameObject.FindGameObjectWithTag("GhostUI").GetComponentInChildren<Image>().color = Color.Lerp(Color.red, Color.white, ColorLerpTimer);
            if(ColorLerpTimer < 1)
            {
                ColorLerpTimer += Time.deltaTime / StunTime;
            }
        }

        if(Input.GetKeyDown(KeyCode.O))
        {
            GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().resetGame(true);
        }
        
        
	}

	public void Die()
	{
		AudioClip scream = deathScreams [Random.Range (0, deathScreams.Length)];
		//PlayGlobalSoundWithCallback (scream, AudioFinished_EndGame);
		GetComponent<PhotonView> ().RPC ("networkPlaySpookySound", PhotonTargets.AllBufferedViaServer, scream.name);
	}


	public delegate void AudioCallback();
	
	void AudioFinished_EndGame()
	{
		Debug.Log ("Sound ended!");
		GameObject.Find ("_NETWORKED_SCRIPTS").GetComponent<RoundManager> ().resetGame (false);
	}
	
	void PlaySoundWithCallback(AudioClip clip, AudioCallback callback)
	{
		GetComponent<AudioSource> ().clip = clip;
		GetComponent<AudioSource> ().Play ();
		StartCoroutine (DelayedCallback (clip.length, callback));
	}

	void PlayGlobalSoundWithCallback(AudioClip clip, AudioCallback callback)
	{
		/*AudioSource globalSound = GameObject.Find ("_GLOBAL_AMBIENT_SPEAKER").GetComponent<AudioSource>();
		globalSound.clip = clip;
		globalSound.Play ();*/
		GetComponent<PhotonView> ().RPC ("networkPlaySpookySound", PhotonTargets.AllBufferedViaServer, clip.name);
		StartCoroutine (DelayedCallback (clip.length, callback));
	}
	
	private IEnumerator DelayedCallback(float time, AudioCallback callback)
	{
		yield return new WaitForSeconds (time);
		callback ();
	}

	public static void SetLayerRecursively(GameObject go, int layerNumber) {
		//if (go == null) return;
		//foreach (Transform trans in go.GetComponentsInChildren<Transform>(true)) {
		//	trans.gameObject.layer = layerNumber;
		//}
	}

	void startVisibilityCooldown()
	{
		//invisibilityTimer = invisibilityTimerMax;
		SetLayerRecursively (gameObject, 15);
		GetComponent<PhotonView>().RPC("networkToggleInvisibility", PhotonTargets.AllBufferedViaServer, isInvisible);
	}

	void startSoundCooldown()
	{
		soundTimer = soundTimerMax;
		soundPlayed = true;
	}

	void startLanternCooldown()
	{
		lanternTimer = 0;
		lanternOff = true;

		ColdAura ();
	}

	void computeCooldown()
	{
		if (!isInvisible)
		{
            //decrement or turn back invisible
			if (invisibilityTimer > 0) invisibilityTimer -= Time.deltaTime;
			else
			{
                TurnInvisible();
			}
		}
		else if (isInvisible)
		{
			invisibilityTimer += Time.deltaTime;
			if (invisibilityTimer >= invisibilityTimerMax) invisibilityTimer = invisibilityTimerMax;
		}

		if (soundPlayed && soundTimer > 0) soundTimer -= Time.deltaTime;
		else if (soundPlayed && soundTimer <= 0)
		{
			soundTimer = 0;
			soundPlayed = false;
		}

		if (!soundPlayed && soundTimer < soundTimerMax) soundTimer += Time.deltaTime;
		else if (!soundPlayed && soundTimer >= soundTimerMax) soundTimer = soundTimerMax;

		if (lanternOff)
		{
			lanternTimer -= Time.deltaTime;
			if (lanternTimer < 0)
			{
				lanternTimer = 0;
				lanternOff = false;
			}
		}
		else
		{
			lanternTimer += Time.deltaTime;
			if (lanternTimer > lanternTimerMax) lanternTimer = lanternTimerMax;
		}

        if(WailOnCoolDown)
        {
            wailCoolDown += Time.deltaTime;
            if (wailCoolDown > wailTimerMax)
            {
                canWail = true;
                wailCoolDown = wailTimerMax;
            }
        }
	}

    private void TurnInvisible()
    {
        GetComponent<PhotonView>().RPC("networkToggleInvisibility", PhotonTargets.OthersBuffered, isInvisible);

        if (isInvisible)
        {
            ghostGraphics.SetActive(true);
        }
        else
        {
            ghostGraphics.SetActive(false);
        }
        //this.gameObject.GetComponentInChildren<Renderer>().material.color = tempColor ;
        isInvisible = !isInvisible;

        GameObject.FindGameObjectWithTag("GhostUI").GetComponentInChildren<Image>().enabled = !GameObject.Find("GhostUI(Clone)").GetComponentInChildren<Image>().enabled;
        GetComponent<Footsteps>().enabled = !GetComponent<Footsteps>().enabled;
        SetLayerRecursively(gameObject, 8);
    }

	void drawCooldown()
	{
		visCdSlider.value = invisibilityTimer;
        WailCooldDownSlider.value = wailCoolDown;
		lanternCdSlider.value = lanternTimer;
        drawHumanSliders();

	}

    void drawHumanSliders()
    {

        bool disableSliders = false;
        
       // if (GameObject.Find ("PlayerControllerHuman(Clone)")) 
       // {
		//	RaycastHit hit;
		//	Vector3 ghost2humanPos = GameObject.FindGameObjectWithTag ("Human").transform.position + Vector3.up - this.transform.position;
		//	if (Vector3.Angle (this.transform.forward, ghost2humanPos) < 90) {
		//		if (Physics.Raycast (this.transform.position, ghost2humanPos, out hit, 1000000.0f)) {
		//			if (hit.collider.gameObject.tag == "Human") {
		//				humanHealthSlider.value = GameObject.FindGameObjectWithTag ("Human").GetComponent<HumanControls> ().hp;
		//				humanHealthSlider.transform.position = ghostCamera.WorldToScreenPoint (GameObject.FindGameObjectWithTag ("Human").transform.position + Vector3.up * 2.5f);
		//				humanHealthSlider.transform.localScale = new Vector3 (.5f, 1, 1);
       //
		//				//humanComfortSlider.value = GameObject.FindGameObjectWithTag ("Human").GetComponent<HumanControls> ().comfort;
		//				humanComfortSlider.transform.position = ghostCamera.WorldToScreenPoint (GameObject.FindGameObjectWithTag ("Human").transform.position + Vector3.up * 2.75f);
		//				humanComfortSlider.transform.localScale = new Vector3 (.5f, 1, 1);
       //                 Debug.Log("Slider Value: " + humanComfortSlider.value);
       //                 //Debug.Log("Player Value: " + GameObject.FindGameObjectWithTag("Human").GetComponent<HumanControls>().comfort);
		//			} 
       //             else 
       //             {
       //
       //                 disableSliders = true;
		//			}
		//		}
		//	} 
		//} 
		//else 
		//{
       //     disableSliders = true;
       //
		//}
       //
       //
       // if(disableSliders)
       // {
       //     DisableSlider(humanHealthSlider);
       //     DisableSlider(humanComfortSlider);
       // }
       // else
       // {
       //     EnableSlider(humanHealthSlider);
       //     EnableSlider(humanComfortSlider);
       // }
    }


    private void DisableSlider(Slider slider)
    {
        slider.transform.Find ("Fill Area/Fill").GetComponent<Image> ().enabled = false;
        slider.transform.Find ("Background").GetComponent<Image> ().enabled = false;
    }

    private void EnableSlider(Slider slider)
    {
        slider.transform.Find ("Fill Area/Fill").GetComponent<Image> ().enabled = true;
        slider.transform.Find ("Background").GetComponent<Image> ().enabled =     true;
    }

	void ChangeUI()
	{
		if (GameObject.Find("OptionsMenu") != null)
		{
			sensitivityX = GameObject.Find("OptionsMenu/XSensitivity").GetComponent<Slider>().value;
			sensitivityY = GameObject.Find("OptionsMenu/YSensitivity").GetComponent<Slider>().value;
		}
	}

	void HandleInput()
	{
		//menu stuff
		GameObject optionsMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("OptionsMenu");
		GameObject pauseMenu = GameObject.Find("_SCRIPTS").GetComponent<MenuHandler>().FindMenu("PauseMenu");

        mouseInput();

        keyboardInput(optionsMenu, pauseMenu);
        
	}

    private void mouseInput()
    {
        //Mouse
        float dx = Input.GetAxis("Mouse X");
        dx = dx * Mathf.Abs(dx);
        dx = Mathf.Clamp(dx, -.5f, .5f);
        float dy = Input.GetAxis("Mouse Y");
        dy = dy * Mathf.Abs(dy);
        dy = Mathf.Clamp(dy, -.5f, .5f);

        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + dx * sensitivityX;
            rotationY += dy * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
            ghostCamera.transform.localEulerAngles = new Vector3(-rotationY, 0, 0);
            transform.localEulerAngles = new Vector3(0, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, dx * sensitivityX, 0);
        }
        else
        {
            rotationY += dy * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
            ghostCamera.transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }

        if (Input.GetMouseButton(1) && canWail )
        {
            GetComponent<GhostWail>().SuckIn();
        }

        if (Input.GetMouseButtonUp(1) && canWail)
        {
            wailCoolDown = 0.0f;
            GetComponent<GhostWail>().Launch();
            canWail = false;
        }
    }

    private void keyboardInput(GameObject optionsMenu, GameObject pauseMenu)
    {
        //Keyboard
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pauseMenu.GetActive() && !optionsMenu.GetActive())
            {
                pauseMenu.SetActive(true);
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
            }
            else if (pauseMenu.GetActive())
            {
                pauseMenu.SetActive(false);
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
            }
            else if (optionsMenu.GetActive())
            {
                optionsMenu.SetActive(false);
                pauseMenu.SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.F))
        {

            if(ghostPower >= 1 && ghostPower <6)
            {
                // if timer off cooldown
                GameObject[] humans = GameObject.FindGameObjectsWithTag("Human");
                if (!lanternOff && lanternTimer == lanternTimerMax) startLanternCooldown();

                foreach (GameObject go in humans)
                {
                  go.GetComponent<HumanControls>().receiveEMPBlast();
                }
            }
            if(ghostPower >= 6)
			    if (lanternTimer == lanternTimerMax) startLanternCooldown();
        }

        if (Input.GetKeyDown(KeyCode.G))
        {

        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if(ghostPower == 3)
            {
                if (canVisible && isInvisible && invisibilityTimer == invisibilityTimerMax)
                {
                    GameObject.FindGameObjectWithTag("GhostUI").GetComponentInChildren<Image>().enabled = !GameObject.Find("GhostUI(Clone)").GetComponentInChildren<Image>().enabled;
                    GetComponent<Footsteps>().enabled = !GetComponent<Footsteps>().enabled;
                    startVisibilityCooldown();
                    //GetComponent<PhotonView>().RPC ("networkToggleInvisibility", PhotonTargets.AllBufferedViaServer, isInvisible);
                }
            }
            else if(ghostPower >= 4)
            {

                if (canVisible && isInvisible)
                {
                    GameObject.FindGameObjectWithTag("GhostUI").GetComponentInChildren<Image>().enabled = !GameObject.Find("GhostUI(Clone)").GetComponentInChildren<Image>().enabled;
                    GetComponent<Footsteps>().enabled = !GetComponent<Footsteps>().enabled;
                    startVisibilityCooldown();
                    //GetComponent<PhotonView>().RPC ("networkToggleInvisibility", PhotonTargets.AllBufferedViaServer, isInvisible);
                }
                else if (!isInvisible)
                {
                    GetComponent<PhotonView>().RPC("networkToggleInvisibility", PhotonTargets.AllBufferedViaServer, isInvisible);
				    GameObject.FindGameObjectWithTag ("GhostUI").GetComponentInChildren<Image> ().enabled = !GameObject.Find ("GhostUI").GetComponentInChildren<Image> ().enabled;
				    GetComponent<Footsteps>().enabled = !GetComponent<Footsteps>().enabled;
				    SetLayerRecursively (gameObject, 8);
                }
            }

        }
    }

	private void ColdAura()
	{
		// Projects an aura of cold, disabling all of the lights nearby temporarily.
		GameObject[] lights = GameObject.FindGameObjectsWithTag ("LightSource");
		List<GameObject> closeLights = new List<GameObject>();

		for (int i = 0; i < lights.Length; i++)
		{
			Vector3 distVec = gameObject.transform.position - lights[i].transform.position;
			Debug.Log(distVec.magnitude);

			if (distVec.magnitude < lightRadius) closeLights.Add(lights[i]);
		}
		
		foreach (GameObject l in closeLights)
		{
			l.GetComponent<NetworkLightSource>().DisableLight();
		}
		
	}

	public void respawn(){
		GameObject [] spawnPoints = GameObject.FindGameObjectsWithTag("GhostSpawn");
		if (spawnPoints == null) {
			Debug.Log ("No spawn points in level!");
			return;
		}
		GameObject mySpawn = spawnPoints [Random.Range (0, spawnPoints.Length)];

        this.GetComponent<CharacterMotor>().enabled = false;
        StartCoroutine(waitThenCallback(StunTime, () => { this.GetComponent<CharacterMotor>().enabled = true; }));

        this.isStunned = true;
        this.GetComponent<SoundBank>().Play("IWillFindYou");
        this.invisibilityTimer = 0;
        this.lanternTimer = 0;
        this.TurnInvisible();
        this.wailCoolDown = 0;
        Debug.Log(GameObject.FindGameObjectWithTag("GhostUI").GetComponentInChildren<Image>().color);
        GameObject.FindGameObjectWithTag("GhostUI").GetComponentInChildren<Image>().color = Color.red;
        ColorLerpTimer = 0.0f;
		//this.transform.position = mySpawn.transform.position;
		//this.transform.rotation = mySpawn.transform.rotation;
	}

    private IEnumerator waitThenCallback(float time, System.Action callback)
    {
        yield return new WaitForSeconds(time);
        callback();
    }

	public void OnCollisionEnter(Collision other)
	{
		if (other.collider.gameObject.tag == "Body")
		{
            //GetComponent<PhotonView>().RPC("networkRespawn", PhotonTargets.AllBuffered, null);
			Debug.Log("I got hit with a body...");
		}

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Collider>().gameObject.tag == "SafeZone")
		{
			canVisible = false;

			if (!isInvisible)
			{
				SetLayerRecursively (gameObject, 8);
				GameObject.FindGameObjectWithTag ("GhostUI").GetComponentInChildren<Image> ().enabled = !GameObject.Find ("GhostUI").GetComponentInChildren<Image> ().enabled;
				GetComponent<Footsteps>().enabled = !GetComponent<Footsteps>().enabled;
				GetComponent<PhotonView>().RPC("networkToggleInvisibility", PhotonTargets.AllBufferedViaServer, isInvisible);
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<Collider>().gameObject.tag == "SafeZone")
		{
			canVisible = true;
		}
	}

	//Networking

	[RPC]
	public void networkToggleInvisibility(bool invisible){
       // Color tempColor = this.gameObject.GetComponentInChildren<Renderer>().material.color;
        
        if (invisible) {
			ghostGraphics.SetActive(true);
		}
		else{
			ghostGraphics.SetActive(false);
		}
        //this.gameObject.GetComponentInChildren<Renderer>().material.color = tempColor ;
		isInvisible = !isInvisible;
    }

	[RPC]
	public void networkPlaySpookySound(string soundName){
		GetComponent<SoundBank> ().Play (soundName);
		//if (GameObject.FindGameObjectWithTag("Human") != null) GameObject.FindGameObjectWithTag("Human").GetComponent<HumanControls>().comfort -= soundDmg;
	}

    [RPC]
    public void networkRespawn()
    {

        this.respawn();
    }









}
