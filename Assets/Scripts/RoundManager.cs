﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class RoundManager : MonoBehaviour {

    public GameObject gameEnvMgr;
	public GameObject human = null;
	public GameObject ghost = null;
	public GameObject body = null;
	public List<SafeZone> safeZones = new List<SafeZone> ();
	public List<HealthFood> food = new List<HealthFood>();
	public List<GhostMemory> memories = new List<GhostMemory>();

    public void Start()
    {
       
    }
    public void Awake()
    {
        
    }
	
	public void resetGame(bool ghostWon)
    {
        GetComponent<PhotonView>().RPC("networkTranslateGame", PhotonTargets.All, ghostWon);
	}

	public void endGame()
	{
		//resetGame ();
	}

	public void showGameOverMenu(bool ghostWon)
	{
		GameObject.Find ("_SCRIPTS").GetComponent<MenuHandler> ().FindMenu("GameOverMenu").SetActive (true);
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		
		if (ghostWon)
		{
			GameObject.Find ("_MENU_SYSTEM/GameOverMenu/GhostWonText").GetComponent<Text>().enabled = true;
			GameObject.Find ("_MENU_SYSTEM/GameOverMenu/HumanWonText").GetComponent<Text>().enabled = false;
		}
		else{
			GameObject.Find ("_MENU_SYSTEM/GameOverMenu/GhostWonText").GetComponent<Text>().enabled = false;
			GameObject.Find ("_MENU_SYSTEM/GameOverMenu/HumanWonText").GetComponent<Text>().enabled = true;
		}
	}

	public void destroyObjects()
	{

	}

	public void reset()
	{
		if (safeZones.Count != 0)
		{
			foreach(SafeZone s in safeZones)
			{
				s.Reset();
			}
		}
		if (food.Count != 0)
		{
			foreach(HealthFood f in food)
			{
				f.Reset();
			}
		}
		if (memories.Count != 0)
		{
			foreach(GhostMemory m in memories)
			{
				m.Reset();
			}
		}
	}

	[RPC]
	void networkTranslateGame (bool ghostWon) {
		destroyObjects ();
		//reset ();
		showGameOverMenu (ghostWon);
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
