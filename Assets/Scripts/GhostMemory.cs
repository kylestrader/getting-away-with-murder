﻿using UnityEngine;
using System.Collections;

public class GhostMemory : MonoBehaviour {

	float powerAmt;
    bool collected = false;
    public int percentToShowUpAt = 100;
	bool humanFound = false;

	// Use this for initialization
	void Start () {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<SphereCollider>().enabled = false;
        gameObject.GetComponent<Light>().enabled = false;
       
	}
	
    void Awake()
    {
        powerAmt = GameObject.Find("_SCRIPTS").GetComponent<Variables>().MemoryPowerAmt;
        GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().memories.Add(this);
    }
	// Update is called once per frame
	void Update () {
	}
	
	public void Reset()
	{
		gameObject.GetComponent<MeshRenderer>().enabled = true;
		gameObject.GetComponent<SphereCollider>().enabled = true;
		gameObject.GetComponent<Light>().enabled = true;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Ghost")
		{
			other.gameObject.GetComponent<GhostControls>().ghostPower += powerAmt;

			gameObject.GetComponent<MeshRenderer>().enabled = false;
			gameObject.GetComponent<SphereCollider>().enabled = false;
			gameObject.GetComponent<Light>().enabled = false;
            collected = true;
		}
	}
}
