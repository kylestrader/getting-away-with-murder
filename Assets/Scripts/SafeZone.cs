﻿using UnityEngine;
using System.Collections;

public class SafeZone : MonoBehaviour {

	private float comfortHeal;
	private float timeToDrain;
	private float rangeDrainPerTick;

	private float timeSinceLastTick = 0.0f;
	private float timePerTick = 1.0f;

	private float originalRange;

	// Use this for initialization
	void Start () {

		comfortHeal = GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().ComfortHeal;
		timeToDrain = GameObject.Find ("_SCRIPTS").GetComponent<Variables> ().HealBeforeDrained;

		//originalRange = transform.FindChild ("Point light").GetComponent<Light> ().range;
		float numberOfTicks = timeToDrain / comfortHeal;

		rangeDrainPerTick = originalRange / numberOfTicks;

        GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().safeZones.Add(this);
	}

	public void Reset()
	{
		//transform.FindChild ("Point light").GetComponent<Light> ().range = originalRange;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider other)
	{
		/*if (other.gameObject.tag == "Human")
		{
			if (timeSinceLastTick >= timePerTick)
			{
				if (other.gameObject.GetComponent<HumanControls>().hp <= 100 && transform.FindChild("Point light").GetComponent<Light>().range > 0) other.gameObject.GetComponent<HumanControls>().hp += (int)comfortHeal;
				//transform.FindChild("Point light").GetComponent<Light>().range -= rangeDrainPerTick;
				timeSinceLastTick = 0.0f;
			}
			else timeSinceLastTick += Time.deltaTime;
		}*/
	}
}
