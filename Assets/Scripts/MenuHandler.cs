﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuHandler : MonoBehaviour {
    public List<GameObject> gameMenus;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SwitchToOptions()
	{
		FindMenu("pauseMenu").SetActive (false);
		FindMenu("optionsMenu").SetActive (true);
	}

    public GameObject FindMenu(string name)
    {
        foreach(GameObject g in gameMenus)
        {
            if (g.name == name)
            {
                return g;
            }
        }

        return null;
    }

}
