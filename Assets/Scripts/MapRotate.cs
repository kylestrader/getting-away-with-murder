﻿using UnityEngine;
using System.Collections;

public class MapRotate : MonoBehaviour {

    public float maxMapRotate = -40f;
    public Camera followCam = null;
    private Vector3 mHoldPosition;
    private Vector3 mAwayPosition;
    public bool isActive;

	// Use this for initialization
	void Start () {
        mHoldPosition = transform.localPosition;
        mAwayPosition = new Vector3(0, -2, -1.835f);
        isActive = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (isActive && transform.localPosition != mHoldPosition)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, mHoldPosition, 6 * Time.deltaTime);
        }

        else if (!isActive && transform.localPosition != mAwayPosition)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, mAwayPosition, 6 * Time.deltaTime);
        }

        float camX_Rot = followCam.transform.rotation.eulerAngles.x;
        float mapRotX = 0f;
        if (camX_Rot >= 0f && camX_Rot <= 50f) // This is the modifiable range
        {
            float lookCoef = camX_Rot / 50f;
            mapRotX =  -(maxMapRotate * lookCoef);
        }

        else if (camX_Rot > 50f && camX_Rot <= 90f) // This is the capped range
        {
            mapRotX = 360f - maxMapRotate;
        }

        transform.localRotation = Quaternion.Euler(mapRotX, 180, 0);
	}

    public void PutAway()
    {
        isActive = false;
    }

    public void TakeOut()
    {
        isActive = true;
    }
}
