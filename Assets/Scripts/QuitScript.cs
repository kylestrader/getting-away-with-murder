﻿using UnityEngine;
using System.Collections;

public class QuitScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void _Quit()
	{
		Debug.Log ("Pressed");
        if (PhotonNetwork.connected) PhotonNetwork.Disconnect();
		Application.Quit();
	}
}
