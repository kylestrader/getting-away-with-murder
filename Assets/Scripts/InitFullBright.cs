﻿using UnityEngine;
using System.Collections;

public class InitFullBright : MonoBehaviour {

    public Shader fullBright = null;

	// Use this for initialization
	void Start () {
        GetComponent<Camera>().RenderWithShader(fullBright, "fullBright");
	}
}
