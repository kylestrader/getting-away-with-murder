﻿using UnityEngine;
using System.Collections;

public class Flicker : MonoBehaviour {
	
	private const float flickerTimer = 0.05f;
	float timeTracker = 0.0f;

	// Update is called once per frame
	void Update () {
		if (timeTracker >= flickerTimer) {
			
			GetComponent<Light>().intensity = 2.01f + (float)Random.Range(-20, 20) / 100f;
			timeTracker = 0.0f;
		}
		timeTracker += Time.deltaTime;
	}
}

