﻿using UnityEngine;
using System.Collections;

public class HealthFood : MonoBehaviour {

	int healAmt;

	// Use this for initialization
	void Start () {

	}

    void Awake()
    {

        healAmt = GameObject.Find("_SCRIPTS").GetComponent<Variables>().FoodHeal;
        GameObject.Find("_NETWORKED_SCRIPTS").GetComponent<RoundManager>().food.Add(this);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reset()
	{
        gameObject.SetActive (true);
		//gameObject.GetComponent<MeshRenderer>().enabled = true;
		//gameObject.GetComponent<BoxCollider>().enabled = true;
	}

	void OnTriggerEnter(Collider other)
	{
		Debug.Log ("Collision with " + other.gameObject.tag + "!");
		if (other.gameObject.tag == "Human")
		{
			HumanControls p = other.gameObject.GetComponent<HumanControls>();
			if (p.hp < 100)
			{
				p.hp += healAmt;

				if (p.hp > 100) p.hp = 100;

				gameObject.SetActive(false);
			}
		}
	}
}
