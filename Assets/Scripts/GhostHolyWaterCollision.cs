﻿using UnityEngine;
using System.Collections;

public class GhostHolyWaterCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnParticleCollision(GameObject other)
	{
		if(other.gameObject.tag == "HolyWater")
		{
			GetComponent<PhotonView>().RPC("networkRespawn", PhotonTargets.AllBuffered, null);
			Debug.Log("HOLY WATER HIT GHOST");
		}
	}
}
