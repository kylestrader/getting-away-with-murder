﻿using UnityEngine;
using System.Collections;

public class BasePlayerScript : MonoBehaviour {

	protected CharacterMotor characterMotor;
	public float sensitivityX, sensitivityY = 15.0f;

	// Use this for initialization
	  protected void Start () {
		 characterMotor = this.gameObject.AddComponent<CharacterMotor>();
	}
	
	// Update is called once per frame
	public virtual void Update () {
		Debug.LogError("Base Player Script Update being called");
	}


	public virtual void Interact()
	{
		Debug.LogError("Base Player Script Interact being called");
	}

	protected virtual void CheckInput()
	{
		Debug.LogError("Base Player Script Input being called");
	}
}
