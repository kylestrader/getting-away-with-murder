﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class Variables : MonoBehaviour {
	#region Traps
	public int MirrorDmg;
	#endregion
	
	#region Human
	public float MaxHealth;
	public float HealthRegen;
	public float BodyThrowForce;
	public float LanternActivationSpd;
	public float LanternReactivateCD;
	public int FoodHeal;
	public int MaxNumVials;
	#endregion
	
	#region Ghost
	public float SpookySndCD;
	public float ColdAuraCD;
	public float VisibilityCD;
	public float SpookySndDmg;
	public float VisibilityDmg;
	public float RespawnTime;
	public float VisibilityTime;
	public bool CanInvisibleAtWill;
	public float MemoryPowerAmt;
	public float LightDisableRadius;
	#endregion
	
	#region Body
	public float BloodDripSpeed;
	#endregion
	
	#region SafeZone
	public float ComfortHeal;
	public float HealBeforeDrained;
	#endregion

	VariableData data;

	// Use this for initialization
	void Start () {

		data = LoadFromFile ("Assets/Variables.xml");
		loadData ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void loadData()
	{
		MirrorDmg = data.MirrorDmg;
		MaxHealth = data.MaxHealth;
		HealthRegen = data.HealthRegen;
		BodyThrowForce = data.BodyThrowForce;
		LanternActivationSpd = data.LanternActivationSpd;
		LanternReactivateCD = data.LanternReactivateCD;
		FoodHeal = data.FoodHeal;
		MaxNumVials = data.MaxNumVials;
		SpookySndCD = data.SpookySndCD;
		ColdAuraCD = data.ColdAuraCD;
		VisibilityCD = data.VisibilityCD;
		RespawnTime = data.RespawnTime;
		VisibilityTime = data.VisibilityTime;
		CanInvisibleAtWill = data.CanInvisibleAtWill;
		MemoryPowerAmt = data.MemoryPowerAmt;
		LightDisableRadius = data.LightDisableRadius;
		BloodDripSpeed = data.BloodDripSpeed;
		ComfortHeal = data.ComfortHeal;
		HealBeforeDrained = data.HealBeforeDrained;
	}

	public static VariableData LoadFromFile(string path)
	{
		var serializer = new XmlSerializer (typeof(VariableData));
		using(var stream = new FileStream(path, FileMode.Open))
		{
			return serializer.Deserialize(stream) as VariableData;
		}
	}
}