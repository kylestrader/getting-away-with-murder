﻿using UnityEngine;
using System.Collections;

public class HandlePlayerDisconnect : Photon.MonoBehaviour {

	void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		if (PhotonNetwork.isMasterClient)
		{
            //A Human has disconnected
            if (player.GetTeam() == PunTeams.Team.red)
            {
                GetComponent<PlayerCreate>().photonView.RPC("playerLeftRoom", PhotonTargets.All, false);
            }

            //A Ghost has disconnected
            else if (player.GetTeam() == PunTeams.Team.blue)
            {
                GetComponent<PlayerCreate>().photonView.RPC("playerLeftRoom", PhotonTargets.All, true);
            }
		}
	}
}
