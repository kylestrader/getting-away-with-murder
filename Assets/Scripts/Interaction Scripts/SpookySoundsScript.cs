﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class SpookySoundsScript : Interactable {


    public float damagePerTick = .25f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(GetComponent<AudioSource>().isPlaying)
        {
            if (Vector3.Distance(GameObject.FindGameObjectWithTag("Human").transform.position, this.transform.position) < this.GetComponent<AudioSource>().maxDistance)
            {
                GameObject.FindGameObjectWithTag("Human").GetComponent<HumanControls>().hp -= (int)damagePerTick ;
                GameObject.FindGameObjectWithTag("Human").GetComponent<PhotonView>().RPC("updateHealth", PhotonTargets.AllBuffered, null);
            }
        }
	
	}

    public override void Interact(bool isGhost)
    {

            if(GetComponent<AudioSource>().isPlaying)
            {
                GetComponent<PhotonView>().RPC("stopMusic", PhotonTargets.AllBuffered, null);
            }
            else
            {
                GetComponent<PhotonView>().RPC("startMusic", PhotonTargets.AllBuffered, null);
            }



    }

    [RPC]
    public void startMusic()
    {
        this.GetComponent<AudioSource>().Play();
    }

    [RPC]
    public void stopMusic()
    {
        this.GetComponent<AudioSource>().Stop();
    }
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
