﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class FurnitureScript : Interactable
{
    public TrailRenderer trailRenderer;
    public Material TrailMaterial;
    private GameObject ghost;
    public bool isHeld;

    //Gravity delay
    float mDelayTime;
    float mCurTimePassed;
    public bool mGravityDelayed;

    public float networkVelocity {  get;  set; }

    // Use this for initialization
    void Start()
    {
        trailRenderer = gameObject.GetComponent <TrailRenderer>();
        if (trailRenderer == null)
        {
            trailRenderer = GetComponent<TrailRenderer>();
        }
        trailRenderer.time = 0.7f;
        trailRenderer.startWidth = 0.15f ;
        trailRenderer.endWidth = 0f;
        trailRenderer.material = TrailMaterial;
        isHeld = false;

        mDelayTime = 0.0f;
        mCurTimePassed = 0.0f;
        mGravityDelayed = false;
    }

    // Update is called once per frame
    void Update()
    {

        trailRenderer.enabled = this.GetComponent<Rigidbody>().velocity.magnitude > 0.5f;
        networkVelocity = this.GetComponent<Rigidbody>().velocity.magnitude;
        if (mGravityDelayed)
            IterateGravityDelay();
    }

    public void pickThisUp()
    {
        ActivateLocalControl();
        GainOwnership();
    }

    public void FloatTowardsDynamic(Vector3 point, float force)
    {
        this.GetComponent<Rigidbody>().AddForce((point - transform.position) * (force * 500));
    }

    public void FloatTowardsKinematic(Vector3 point, float force)
    {
        transform.position = Vector3.Lerp(transform.position, point, force);
    }

    public void throwObject(Vector3 dir, float force, bool shouldDelayGravityActivation, float timeDelay)
    {
        dropThis(shouldDelayGravityActivation, timeDelay);
        this.GetComponent<Rigidbody>().AddForce(dir.normalized * force);
    }

    private void GainOwnership()
    {
        this.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player.ID);
        this.GetComponent<PhotonView>().RPC("ActivateControl", PhotonTargets.OthersBuffered, null);
    }

    private void dropThis(bool shouldDelayGravityActivation, float timeDelay)
    {
        DeactivateLocalControl(shouldDelayGravityActivation, timeDelay);
        this.GetComponent<PhotonView>().RPC("DeactivateControl", PhotonTargets.OthersBuffered, shouldDelayGravityActivation,timeDelay);
    }

    private void ActivateLocalControl()
    {
        GetComponent<Rigidbody>().drag = 5f;
        GetComponent<Rigidbody>().isKinematic = false;
    }

    [RPC]
    public void ActivateControl()
    {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().drag = 5f;
    }
    private void DeactivateLocalControl(bool shouldDelayGravityActivation, float timeDelay)
    {
        if (shouldDelayGravityActivation)
        {
            GetComponent<Rigidbody>().useGravity = false;
            mGravityDelayed = true;
            DelayGravityActivation(timeDelay);
        }
        else
        {
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<FurnitureScript>().isHeld = false;
        }

        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().freezeRotation = false;
        GetComponent<Rigidbody>().drag = 0f;
    }

    [RPC]
    public void DeactivateControl(bool shouldDelayGravityActivation, float delay)
    {
        GetComponent<FurnitureScript>().isHeld = false;
        mDelayTime = delay;
        mGravityDelayed = shouldDelayGravityActivation;
    }

    [RPC]
    public void NetworkToggleGravity(bool gravityOn)
    {
       // mGravityDelayed = !gravityOn;
    }

    public void DelayGravityActivation(float delay)
    {
        mDelayTime = delay;
        mGravityDelayed = true;
    }

    private void IterateGravityDelay()
    {
        mCurTimePassed += Time.deltaTime;

        if (mCurTimePassed >= mDelayTime)
        {
            mCurTimePassed = 0.0f;
            mDelayTime = 0.0f;
            GetComponent<Rigidbody>().useGravity = true;
            this.GetComponent<PhotonView>().RPC("NetworkToggleGravity", PhotonTargets.OthersBuffered, true);
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
