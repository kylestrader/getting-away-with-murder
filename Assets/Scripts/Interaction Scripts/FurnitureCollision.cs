﻿using UnityEngine;
using System.Collections;

public class FurnitureCollision : MonoBehaviour {

    public AudioClip[] CollisionSounds;
    public float furnitureDamage = 10f;



	// Use this for initialization
	void Start () {
	
        if(Input.GetKeyDown(KeyCode.F3))
        {
            hitByFurniture();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Quote))
        {
            hitByFurniture();
        }
	
	}

    void OnControllerColliderHit(ControllerColliderHit other)
    {
        if (other.gameObject.tag == "Interactable")
        {
            if (other.gameObject.GetComponent<FurnitureScript>() != null)
                if (other.gameObject.GetComponent<FurnitureScript>().networkVelocity >= 1)
                {
                    GetComponent<PhotonView>().RPC("hitByFurniture", PhotonTargets.AllBuffered, null) ;
                }
        }
    }

    [RPC]
    private void hitByFurniture()
    {
        GetComponent<AudioSource>().clip = CollisionSounds[Random.Range(0, CollisionSounds.Length - 1)];
        GetComponent<AudioSource>().Play();
        this.GetComponent<HumanControls>().hp -= (int)furnitureDamage;
        this.GetComponentInChildren<Camera>().GetComponent<CameraShake>().Shake();
    }
}
