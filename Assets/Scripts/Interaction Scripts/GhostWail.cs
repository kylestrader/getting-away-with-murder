﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GhostWail : MonoBehaviour {

    public GameEnvironment gameEnv;
    public float WailAngle = 90.0f;
    public float WailDamage = 5.0f;
    public float PushBackForce = 100.0f;
    public float WailDistance = 10.0f;
    public float SuckInRadius = 5.0f;
    public float SuckInSpeed = 4.0f;
    public GameObject Camera;

    public int maxNumFurniture;

    List<GameObject> hitColliders;

	// Use this for initialization
	void Start () {
        gameEnv = GameObject.Find("_GAME_ENV_MANAGER").GetComponent<GameEnvironment>();
        hitColliders = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!System.Object.ReferenceEquals(hitColliders, null))
        {
            foreach(GameObject obj in hitColliders)
            {
                obj.GetComponent<FurnitureScript>().FloatTowardsDynamic(this.transform.position + (Vector3.up) + (Camera.transform.forward), Time.deltaTime * SuckInSpeed);
            }
        }

	}

    public void SuckIn()
    {
        Collider[] tmp = Physics.OverlapSphere(this.transform.position, SuckInRadius);
        foreach (Collider objects in tmp)
        {
            GameObject theObject = objects.gameObject;
            if (theObject.tag == "Interactable" && theObject.GetComponent<FurnitureScript>() != null && !theObject.GetComponent<FurnitureScript>().isHeld)
            {
                if (hitColliders.Count < maxNumFurniture)
                {
                    theObject.GetComponent<FurnitureScript>().isHeld = true;
                    hitColliders.Add(theObject);
                    theObject.GetComponent<FurnitureScript>().pickThisUp();
                    //theObject.transform.position = Vector3.Lerp(theObject.transform.position, this.transform.position + (Vector3.up * 2) + (Camera.transform.forward), Time.deltaTime * SuckInSpeed);
                }
            }
        }
    }

    public void Launch()
    {
        foreach (var humanID in gameEnv.Humans)
        {
            //Get Vector of ghost to human
            GameObject human = humanID.Value;

            Vector3 human2Ghost = human.transform.position - this.transform.position;

            float angle = Vector3.Angle(human2Ghost, this.transform.forward);
            Debug.Log(angle);

            if (angle < WailAngle && human2Ghost.magnitude < WailDistance)
            {
                human.GetComponent<HumanControls>().hp -= (int)WailDamage;
                human.GetComponent<HumanControls>().pushBack(human2Ghost.normalized, PushBackForce, WailDamage);
            }
        }

        if (!System.Object.ReferenceEquals(hitColliders, null))
        {
            List<GameObject> newList = new List<GameObject>();
            foreach (GameObject objects in hitColliders)
            {
                if (objects.tag == "Interactable")
                {
                    newList.Add(objects);
                    objects.GetComponent<FurnitureScript>().isHeld = false;
                    objects.GetComponent<FurnitureScript>().throwObject(Camera.transform.forward, 1400f, true, 4f);
                }
            }

            foreach (GameObject obj in newList)
            {
                hitColliders.Remove(obj);
            }
        }
    }
}
