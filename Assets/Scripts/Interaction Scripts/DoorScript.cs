﻿using UnityEngine;
using System.Collections;

public class DoorScript : Interactable {

    private bool isLocked;
    public float forceOpeningDoor = 1.0f;
    public bool canOpen = false;
    public int maxAngle;
    public int directionMod = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    public override void Interact(bool isGhost)
    {

        if(isGhost)
        {
            //isLocked = !isLocked;
            //Debug.Log("LOCKED OR UNLOCKED DOOR");
            //this.GetComponent<PhotonView>().RPC("updateDoor", PhotonTargets.AllBufferedViaServer, isLocked);
        }
        else
        {
            if(!isLocked)
            {


                this.GetComponent<PhotonView>().RPC("pushDoor", PhotonTargets.AllBuffered, null);
               

                
            }

        }

        
    }


    public void OnTriggerEnter(Collider other)
    {


    }

    public void OnTriggerLeave(Collider other)
    {

    }

    [RPC]
    public void updateDoor(bool newDoor)
    {
        this.isLocked = newDoor;
    }

    [RPC]
    public void pushDoor()
    {
        JointSpring s = new JointSpring();
        s.spring = this.GetComponent<HingeJoint>().spring.spring;
        s.damper = this.GetComponent<HingeJoint>().spring.damper;
        int pos = (int)this.GetComponent<HingeJoint>().spring.targetPosition;

        if (pos == maxAngle)
        {
            s.targetPosition = 0;
        }
        else if (pos == 0)
        {
            s.targetPosition = maxAngle * directionMod;
        }
        else
        {
            s.targetPosition = 0;
        }

        this.GetComponent<Rigidbody>().velocity = transform.forward * forceOpeningDoor;
        this.GetComponent<HingeJoint>().spring = s;
        Debug.Log("OPENED OR CLOSED DOOR");
    }
}
