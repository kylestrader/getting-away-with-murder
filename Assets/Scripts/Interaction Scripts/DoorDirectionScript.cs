﻿using UnityEngine;
using System.Collections;

public class DoorDirectionScript : MonoBehaviour {

    public int Direction = 1;
    public Vector3 startPos;


	// Use this for initialization
	void Start () {
        startPos = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = startPos;
	}



    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Human")
        {
            this.transform.parent.gameObject.GetComponentInChildren<DoorScript>().directionMod = this.Direction;
            this.transform.parent.gameObject.GetComponentInChildren<DoorScript>().canOpen = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Human")
        {
            this.transform.parent.gameObject.GetComponentInChildren<DoorScript>().canOpen = false;
        }

    }
}
