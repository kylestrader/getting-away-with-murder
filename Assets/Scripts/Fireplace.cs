﻿using UnityEngine;
using System.Collections;

public class Fireplace : MonoBehaviour {

    public bool m_IsActive;

	public int numWoodView;
	public bool hasGasView;
	public bool matchesLitView;

	public struct FireInventory
	{
		public int numWood;
		public bool hasGas;
		public bool matchesLit;
	}
	public FireInventory inventory;

	// Use this for initialization
	void Start () {
        m_IsActive = false;

		inventory.numWood = 0;
		inventory.hasGas = false;
		inventory.matchesLit = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (CheckInventory() && !IsActive())
		{
			GetComponent<PhotonView>().RPC("LightFireplace", PhotonTargets.AllBufferedViaServer);
		}
	}

	bool CheckInventory()
	{
		if (inventory.numWood >= 1 && inventory.hasGas && inventory.matchesLit)
			return true;
		else return false;
	}

	public void HumanInteract(HumanControls player)
	{
		if (player.inventory.hasWood)
		{
			inventory.numWood++;
			player.inventory.hasWood = false;
			player.holdingWood = false;
			Destroy(player.body);
			player.PlayObjectiveSound();
		}
		if (player.inventory.hasTinder)
		{
			inventory.hasGas = true;
			player.inventory.hasTinder = false;
			player.PlayObjectiveSound();
		}
		if (player.inventory.hasMatches)
		{
			inventory.matchesLit = true;
			player.inventory.hasMatches = false;
			player.PlayObjectiveSound();
		}
		numWoodView = inventory.numWood;
		hasGasView = inventory.hasGas;
		matchesLitView = inventory.matchesLit;

		if (inventory.hasGas && inventory.matchesLit && inventory.numWood >= 1)
			player.PlayMatchSound ();

		GetComponent<PhotonView> ().RPC ("updateFireplaceInventory", PhotonTargets.AllBufferedViaServer);
	}

    public void ToggleActive(bool isActive)
    {
        if (!isActive)
        {
            transform.Find("FireComplex").gameObject.SetActive(false);
            m_IsActive = false;
        }
        else
        {
            transform.Find("FireComplex").gameObject.SetActive(true);
            m_IsActive = true;
        }

		Debug.Log (isActive);
    }

    public bool IsActive()
    {
        return m_IsActive;
    }

	[RPC]
	public void LightFireplace()
	{
		ToggleActive (true);
	}

	[RPC]
	public void updateFireplaceInventory()
	{
		inventory.numWood = numWoodView;
		inventory.hasGas = hasGasView;
		inventory.matchesLit = matchesLitView;
	}
}
