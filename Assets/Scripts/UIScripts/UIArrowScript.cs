﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIArrowScript : MonoBehaviour {

    public Image arrow;
    public Vector3 targetLoc;
    public GameObject UI;
    public bool isVisible;
    public Sprite arrowSprite;
    public Sprite onScreenSprite;

	// Use this for initialization
	void Start () {
        
        isVisible = false;
        
	}
	
	// Update is called once per frame
	void Update () {
        
        if(UI == null)
        {
            
            UI = GameObject.Find("HumanUI(Clone)");
            if(UI != null)
            {
                arrow = UI.transform.Find("Arrow").GetComponent<Image>();
                arrow.enabled = false;
            }

        }
        if(isVisible)
        if(targetLoc != null)
        {
            Vector3 screenpos = Camera.main.WorldToScreenPoint(targetLoc);
            
            //if we are on screen
            if(screenpos.x > 0 && screenpos.y > 0 && screenpos.x < Screen.width && screenpos.y < Screen.height && screenpos.z > 0)
            {
                arrow.sprite = onScreenSprite;
                arrow.transform.position = screenpos;
            }
            else
            {
                arrow.sprite = arrowSprite;
                if(screenpos.z <0)
                {
                    screenpos *= -1;
                }

                Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;
                screenpos -= screenCenter;
                float angle = Mathf.Atan2(screenpos.y, screenpos.x);
                angle -= 90 * Mathf.Deg2Rad;

                float cos = Mathf.Cos(angle);
                float sin = Mathf.Sin(angle);
                screenpos = screenCenter + new Vector3(sin*150, cos*150, 0);
                float m = cos / sin;
                Vector3 screenBounds = screenCenter * 0.9f;
                if(cos>0)
                {
                    screenpos = new Vector3(screenBounds.y / m, screenBounds.y, 0);
                }
                else
	            {
                screenpos = new Vector3(-screenBounds.y/m, -screenBounds.y,0);
	            }

                if(screenpos.x > screenBounds.x)
                {
                    screenpos = new Vector3(screenBounds.x, screenBounds.x * m, 0);
                }
                else if(screenpos.x < -screenBounds.x)
                {
                    screenpos = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);
                }

                screenpos += screenCenter;

                arrow.transform.position = screenpos;
                //arrowSprite.transform.position = Camera.main.ScreenToWorldPoint(screenpos);
                arrow.transform.rotation = Quaternion.Euler(0,0,angle*Mathf.Rad2Deg);

            }

        }
	    
        if(Input.GetKeyDown(KeyCode.Minus) && !isVisible)
        {
            setLocation(this.gameObject.transform.position);
            this.GetComponent<PhotonView>().RPC("networkToggleArrow", PhotonTargets.AllBuffered, null);
        }
        
	}

    public void toggleVisibility()
    {
        isVisible = !isVisible;
        arrow.enabled = isVisible;
        if (isVisible)
            Invoke("toggleVisibility", 5.0f);

    }

    public void setLocation(Vector3 position)
    {
        targetLoc = position;
    }
    public void setOnScreenImage(Sprite onScreenSprit)
    {
        onScreenSprite = onScreenSprit;
    }

    [RPC]
    public void networkToggleArrow()
    {
        GameEnvironment gameEnv = GameObject.Find("_GAME_ENV_MANAGER").GetComponent<GameEnvironment>();
        foreach(var human in gameEnv.Humans)
        {
            UIArrowScript arrow = human.Value.GetComponent<UIArrowScript>();
            arrow.setLocation(this.targetLoc);
            arrow.toggleVisibility();
        }
    }
}
