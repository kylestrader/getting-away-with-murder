﻿using UnityEngine;
using System.Collections;

public class DisconnectUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void DisconnectAndReturnToMenu()
    {
        GameObject.Find("_SCRIPTS").GetComponent<NetworkManager>().Disconnect();
    }
}
