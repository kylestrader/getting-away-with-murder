﻿using UnityEngine;
using System.Collections;
public class AddToMenuHandler : MonoBehaviour {


    private GameObject scriptsReference;

	// Use this for initialization
	void Start () {
        scriptsReference = GameObject.Find("_SCRIPTS");

        //Check to see if _SCRIPTS object exists
        if(System.Object.ReferenceEquals(null,scriptsReference))
        {
            Debug.LogError("SCRPITS Object not found");
        }
        else
        {
            scriptsReference.GetComponent<MenuHandler>().gameMenus.Add(this.transform.gameObject);
            Debug.Log(this.gameObject.GetType().Name + " added to  MenuHandler");

            //If it's the mainMenu turn itself on
            this.gameObject.SetActive(this.gameObject.name == "MainMenu");
        }

	}
}
