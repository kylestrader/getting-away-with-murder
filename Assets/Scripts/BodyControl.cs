﻿using UnityEngine;
using System.Collections;

public class BodyControl : MonoBehaviour {

	public bool isHeld = false;
	private GameObject [] bodySpawnPoints;
	private Vector3 pointToFloatTowards;
	private Quaternion rotationToLookTowards;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isHeld) {
			//transform.GetComponent<Rigidbody>().AddForce((( pointToFloatTowards - transform.position).normalized * (300 * (Vector3.Distance(pointToFloatTowards, transform.position)/7))));
			transform.position = Vector3.Slerp(transform.position, pointToFloatTowards, 0.1f);
			if (!gameObject.GetComponent<Rigidbody>().freezeRotation){
				transform.rotation = rotationToLookTowards;

				if (transform.rotation == rotationToLookTowards){
					//gameObject.rigidbody.freezeRotation = true;
				}
			}
			//Vector3 force = (transform.position - pointToFloatTowards).normalized;
			//transform.rigidbody.AddForce (force);
		}

		else if (gameObject.GetComponent<Rigidbody>().freezeRotation){
			gameObject.GetComponent<Rigidbody>().freezeRotation = false;
		}
	}

	public void setPointToFloatTowards(Vector3 point){
		pointToFloatTowards = point;
        Debug.Log(point);
	}

	public void setRotationToLookTowards(Quaternion look){
		rotationToLookTowards = look;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Spooky")
		{
			GameObject.Find("SpookyScarySkelly").GetComponent<AudioSource>().Play();
		}
	}

	void OnCollisionEnter(Collision c)
	{

	}

	void GhostDies()
	{
		Debug.Log ("Human won!");
		GameObject.Find ("_NETWORKED_SCRIPTS").GetComponent<RoundManager> ().resetGame (false);
	}

	public void respawn()
	{
		bodySpawnPoints = GameObject.FindGameObjectsWithTag("BodySpawn");
		GameObject bodySpawn = bodySpawnPoints [Random.Range (0, bodySpawnPoints.Length)];
		
		if (bodySpawn){
			transform.position = bodySpawn.transform.position;
			transform.rotation = bodySpawn.transform.rotation;
			transform.parent = null;
			isHeld = false;
		}
	}
}
