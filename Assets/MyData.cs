﻿using UnityEngine;
using System.Collections;

public class MyData : MonoBehaviour {

    public GameObject gameEnvMgr;
    private GameObject m_MyPlayer;
    private bool m_IsGhost;
    private bool m_HasSpawned;

    public void Start()
    {
        m_HasSpawned = false;
    }

    public void Instantiate(bool isGhost, GameObject myPlayer)
    {
        m_HasSpawned = true;
        m_IsGhost = isGhost;
        m_MyPlayer = myPlayer;
    }

    public void HandleDisconnect()
    {
        if (m_HasSpawned)
        {
            m_HasSpawned = false;
            if (m_IsGhost)
            {
                gameEnvMgr.GetComponent<GameEnvironment>().removeGhost(m_MyPlayer.GetComponent<GameId>().ID);
            }

            else
            {
                gameEnvMgr.GetComponent<GameEnvironment>().removeHuman(m_MyPlayer.GetComponent<GameId>().ID);
            }
        }
    }
}
