﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
[RequireComponent(typeof(PhotonView))]
public class HolyWaterProjectile : MonoBehaviour {

	bool particlePlayed = false;
    public AudioClip[] sfx;

	// Use this for initialization
	void Start () {

        this.GetComponent<PhotonView>().viewID = PhotonNetwork.AllocateViewID();
	
	}
	
	// Update is called once per frame
	void Update () {

        if (particlePlayed && !GetComponentInChildren<ParticleSystem>().IsAlive())
		{
			Destroy(gameObject);
		}
	
	}

	void OnCollisionEnter(Collision other)
	{
        this.GetComponent<PhotonView>().RPC("networkPlaySounds",PhotonTargets.AllBufferedViaServer,null);
        GetComponentInChildren<ParticleSystem>().Play();
		GetComponentInChildren<MeshRenderer> ().enabled = false;
        this.GetComponent<CapsuleCollider>().enabled = false;
		particlePlayed = true;
	}

    [RPC]
    public void networkPlaySounds()
    {
        GetComponent<AudioSource>().clip = sfx[Random.Range(0, sfx.Length - 1)];
        GetComponent<AudioSource>().Play();
    }
}
